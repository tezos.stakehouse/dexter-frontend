function slippage(x,y) {return Math.abs(x-y) / y;}

slippage(0.01310981, 0.013195799096223862)
slippage(0.01315, 0.013195799096223862)
slippage(0.0131, 0.013195799096223862)
slippage(0.01, 0.013195799096223862)


xtz in 1.000000
xtz pool 28170.283231
token pool 3.71729398
exchangeRates 0.013155
marketRate 0.013195799096223862
slippage 0.3091824597082318

xtz in 0.100000
xtz pool 28170.283231
token pool 3.71729398
exchangeRate 0.01315
marketRate 0.013195799096223862
slippage 0.3470733063598045

xtz in 0.0100000
xtz pool 28170.283231
token pool 3.71729398
exchangeRates 0.0131
marketRate 0.013195799096223862
slippage 0.7259817728755444


xtz in 0.000100
xtz pool 28170.283231
token pool 3.71729398
exchangeRates 0.01
marketRate 0.013195799096223862
slippage 24.218306696851563


https://github.com/ianlapham/uniswap-frontend/blob/5e61b941568a620e3b1e5ff894a2ddc4a228e59a/src/components/ExchangePage/index.jsx

var bigInt = require('big-integer');

function exchangeRate(inputValue, inputDecimals, outputValue, outputDecimals, invert = false) {
  var factor = bigInt(10).pow(18);
  if (invert) {
    return bigInt(inputValue)
           .times(factor)
           .times(bigInt(10).pow(outputDecimals))
           .divide(bigInt(10).pow(inputDecimals))
           .divide(outputValue);
  } else {
    return bigInt(outputValue)
           .times(factor)
           .times(bigInt(10).pow(inputDecimals))
           .divide(bigInt(10).pow(outputDecimals))
           .divide(inputValue);
  };
};

exchangeRate('28170283231', '6', '371729398', '8')

131957990962238n

exchangeRate('28170283231', '6', '371729398', '8', true)

7578169330314843702515

1319579.90962238
13195.7990962238
1319579909.62238

8 + 6 + 2
0.0131957990962238


function calculateSlippage(exchangeRate, marketRate) {
  return bigInt(exchangeRate)
         .minus(bigInt(marketRate))
         .abs()
         .times(bigInt(10).pow(6))
         .divide(bigInt(marketRate))
         .minus(bigInt(3).times(bigInt(10).pow(3)));
};

calculateSlippage('131957990962238','7578169330314843702515');

996999

amountFormatter('996999', 4, 2)

const percentSlippageFormatted = percentSlippage && amountFormatter(percentSlippage, 16, 2)


const percentSlippage =
  exchangeRate && marketRate && !marketRate.isZero()
    ? exchangeRate
        .sub(marketRate)
        .abs()
        .mul(ethers.utils.bigNumberify(10).pow(ethers.utils.bigNumberify(18)))
        .div(marketRate)
        .sub(ethers.utils.bigNumberify(3).mul(ethers.utils.bigNumberify(10).pow(ethers.utils.bigNumberify(15))))
    : undefined


function getExchangeRate(inputValue, inputDecimals, outputValue, outputDecimals, invert = false) {
  try {
    if (
      inputValue &&
      (inputDecimals || inputDecimals === 0) &&
      outputValue &&
      (outputDecimals || outputDecimals === 0)
    ) {
      const factor = ethers.utils.bigNumberify(10).pow(ethers.utils.bigNumberify(18))

      if (invert) {
        return inputValue
          .mul(factor)
          .mul(ethers.utils.bigNumberify(10).pow(ethers.utils.bigNumberify(outputDecimals)))
          .div(ethers.utils.bigNumberify(10).pow(ethers.utils.bigNumberify(inputDecimals)))
          .div(outputValue)
      } else {
        return outputValue
          .mul(factor)
          .mul(ethers.utils.bigNumberify(10).pow(ethers.utils.bigNumberify(inputDecimals)))
          .div(ethers.utils.bigNumberify(10).pow(ethers.utils.bigNumberify(outputDecimals)))
          .div(inputValue)
      }
    }
  } catch {}
}

function getMarketRate(
  swapType,
  inputReserveETH,
  inputReserveToken,
  inputDecimals,
  outputReserveETH,
  outputReserveToken,
  outputDecimals,
  invert = false
) {
  if (swapType === ETH_TO_TOKEN) {
    return getExchangeRate(outputReserveETH, 18, outputReserveToken, outputDecimals, invert)
  } else if (swapType === TOKEN_TO_ETH) {
    return getExchangeRate(inputReserveToken, inputDecimals, inputReserveETH, 18, invert)
  } else if (swapType === TOKEN_TO_TOKEN) {
    const factor = ethers.utils.bigNumberify(10).pow(ethers.utils.bigNumberify(18))
    const firstRate = getExchangeRate(inputReserveToken, inputDecimals, inputReserveETH, 18)
    const secondRate = getExchangeRate(outputReserveETH, 18, outputReserveToken, outputDecimals)
    try {
      return !!(firstRate && secondRate) ? firstRate.mul(secondRate).div(factor) : undefined
    } catch {}
  }
}


const percentSlippage =
  exchangeRate && marketRate && !marketRate.isZero()
    ? exchangeRate
        .sub(marketRate)
        .abs()
        .mul(ethers.utils.bigNumberify(10).pow(ethers.utils.bigNumberify(18)))
        .div(marketRate)
        .sub(ethers.utils.bigNumberify(3).mul(ethers.utils.bigNumberify(10).pow(ethers.utils.bigNumberify(15))))
    : undefined

abs(exchangeRate - marketRate) * (10**18) / marketRate - (3 * 10**15)

const percentSlippageFormatted = percentSlippage && amountFormatter(percentSlippage, 16, 2)


export function amountFormatter(amount, baseDecimals = 18, displayDecimals = 3, useLessThan = true) {
  if (baseDecimals > 18 || displayDecimals > 18 || displayDecimals > baseDecimals) {
    throw Error(`Invalid combination of baseDecimals '${baseDecimals}' and displayDecimals '${displayDecimals}.`)
  }

  // if balance is falsy, return undefined
  if (!amount) {
    return undefined
  }
  // if amount is 0, return
  else if (amount.isZero()) {
    return '0'
  }
  // amount > 0
  else {
    // amount of 'wei' in 1 'ether'
    const baseAmount = ethers.utils.bigNumberify(10).pow(ethers.utils.bigNumberify(baseDecimals))

    const minimumDisplayAmount = baseAmount.div(
      ethers.utils.bigNumberify(10).pow(ethers.utils.bigNumberify(displayDecimals))
    )

    // if balance is less than the minimum display amount
    if (amount.lt(minimumDisplayAmount)) {
      return useLessThan
        ? `<${ethers.utils.formatUnits(minimumDisplayAmount, baseDecimals)}`
        : `${ethers.utils.formatUnits(amount, baseDecimals)}`
    }
    // if the balance is greater than the minimum display amount
    else {
      const stringAmount = ethers.utils.formatUnits(amount, baseDecimals)

      // if there isn't a decimal portion
      if (!stringAmount.match(/\./)) {
        return stringAmount
      }
      // if there is a decimal portion
      else {
        const [wholeComponent, decimalComponent] = stringAmount.split('.')
        const roundedDecimalComponent = ethers.utils
          .bigNumberify(decimalComponent.padEnd(baseDecimals, '0'))
          .toString()
          .padStart(baseDecimals, '0')
          .substring(0, displayDecimals)

        // decimals are too small to show
        if (roundedDecimalComponent === '0'.repeat(displayDecimals)) {
          return wholeComponent
        }
        // decimals are not too small to show
        else {
          return `${wholeComponent}.${roundedDecimalComponent.toString().replace(/0*$/, '')}`
        }
      }
    }
  }
}




token_pool = 8.0
xtz_pool = 40000.00

exchange_rate = token_pool / xtz_pool
market_rate = xtz_pool / token_pool
slippage = (Math.abs(exchange_rate - market_rate) / market_rate) * 0.997

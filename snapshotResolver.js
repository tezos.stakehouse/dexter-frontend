const path = require('path')

const resolveSnapshotPath = (testPath, snapshotExtension) => {
  // Get source path of test file (assuming the build
  // output is stored in lib/)
  const testSourcePath = testPath.replace('lib/js/', '')
  // Get directory of test source file
  const testDirectory = path.dirname(testSourcePath)
  // Get file name of test source file
  const testFilename = path.basename(testSourcePath)
  // Construct file path for snapshot, saved next to original test source file
  return `${testDirectory}/__snapshots__/${testFilename}${snapshotExtension}`
}

const resolveTestPath = (snapshotFilePath, snapshotExtension) => {
  // Transform snapshot file path
  const testSourceFile = snapshotFilePath
    // Remove __snapshot__ directory
    .replace(`/__snapshots__`, '')
    // Remove snapshot extension so we end up with .js
    .replace(snapshotExtension, '')
  // Return test file path in lib directory
  return `lib/js/${testSourceFile}`
}

module.exports = {
  resolveSnapshotPath,
  resolveTestPath,
  // This is an example test file path to guarantee internal consistency
  // Will be used to check if the functions above work properly
  testPathForConsistencyCheck:
    'lib/js/__tests__/DexterUi/DexterUi_WalletInfo/DexterUi_Status_Spec.js'
}

~/alphanet.sh client originate contract tezosGold for alice \
                                 transferring 0 from alice \
                                 running container:contracts/token.tz \
                                 --init '(Pair {Elt "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH" (Pair 1000000 {})} (Pair 1000000 (Pair "Tezos Gold" "TGD")))' --burn-cap 2 --force

~/alphanet.sh client originate contract tezosSilver for alice \
                                 transferring 0 from alice \
                                 running container:contracts/token.tz \
                                 --init '(Pair {Elt "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH" (Pair 1000000 {})} (Pair 1000000 (Pair "Tezos Silver" "TSD")))' --burn-cap 2 --force

~/alphanet.sh client originate contract tastyTacos for alice \
                                 transferring 0 from alice \
                                 running container:contracts/token.tz \
                                 --init '(Pair {Elt "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH" (Pair 1000000 {})} (Pair 1000000 (Pair "Tasty Tacos" "TTS")))' --burn-cap 2 --force

~/alphanet.sh client originate contract checkers for alice \
                                 transferring 0 from alice \
                                 running container:contracts/token.tz \
                                 --init '(Pair {Elt "tz1SeGZ8JpFwSEYVAZo9NjjrLyPKyBwT8kRB" (Pair 500000 {}); Elt "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH" (Pair 500000 {})} (Pair 1000000 (Pair "Checkers" "CHECK")))' --burn-cap 2 --force

~/alphanet.sh client originate contract wrappedLTC for alice \
                                 transferring 0 from alice \
                                 running container:contracts/token.tz \
                                 --init '(Pair {Elt "tz1SeGZ8JpFwSEYVAZo9NjjrLyPKyBwT8kRB" (Pair 400000 {}); Elt "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH" (Pair 377000 {})} (Pair 777000 (Pair "Wrapped LTC" "WLTC")))' --burn-cap 2 --force



~/alphanet.sh client originate contract broker for alice \
                               transferring 0 from alice \
                               running container:contracts/broker.tz \
                               --init '(Pair {} Unit)' --burn-cap 2 --force


~/alphanet.sh client originate contract tezosGoldExchange for alice \
                                 transferring 0 from alice \
                                 running container:contracts/exchange.tz \
                                 --init '(Pair {} (Pair (Pair "KT1XAMU1kn8EJLM2uqrP71Jevvkyo7yyfNTK" "KT1DmCHxit2bQ2GiHVc24McY6meuJPMTrqD8") (Pair 0 {})))' --burn-cap 10 --force


~/alphanet.sh client originate contract tezosSilverExchange for alice \
                                 transferring 0 from alice \
                                 running container:contracts/exchange.tz \
                                 --init '(Pair {} (Pair (Pair "KT1XAMU1kn8EJLM2uqrP71Jevvkyo7yyfNTK" "KT1GutmbJNisDvN2XiuKLdoRHukSfh5bsUUc") (Pair 0 {})))' --burn-cap 10 --force

~/alphanet.sh client originate contract tastyTacosExchange for alice \
                                 transferring 0 from alice \
                                 running container:contracts/exchange.tz \
                                 --init '(Pair {} (Pair (Pair "KT1XAMU1kn8EJLM2uqrP71Jevvkyo7yyfNTK" "KT1AszhGevBf3EFP5EhxaR4i15oXpqzD5kaE") (Pair 0 {})))' --burn-cap 10 --force

~/alphanet.sh client originate contract checkersExchange for alice \
                                 transferring 0 from alice \
                                 running container:contracts/exchange.tz \
                                 --init '(Pair {} (Pair (Pair "KT1XAMU1kn8EJLM2uqrP71Jevvkyo7yyfNTK" "KT1F3D8aAeiYA6EWu96AJhES1zRYJPm4UpiM") (Pair 0 {})))' --burn-cap 10 --force


~/alphanet.sh client originate contract wrappedLTCExchange for alice \
                                 transferring 0 from alice \
                                 running container:contracts/exchange.tz \
                                 --init '(Pair {} (Pair (Pair "KT1XAMU1kn8EJLM2uqrP71Jevvkyo7yyfNTK" "KT1AfhJ6kxvtqYMRJ2uzq3xSiH9cvHBENGSV") (Pair 0 {})))' --burn-cap 10 --force


~/alphanet.sh client list known contracts


broker:      KT1XAMU1kn8EJLM2uqrP71Jevvkyo7yyfNTK
tezosGold:   KT1DmCHxit2bQ2GiHVc24McY6meuJPMTrqD8
tezosSilver: KT1GutmbJNisDvN2XiuKLdoRHukSfh5bsUUc
tastyTacos:  KT1AszhGevBf3EFP5EhxaR4i15oXpqzD5kaE
checkers:    KT1F3D8aAeiYA6EWu96AJhES1zRYJPm4UpiM
wrappedLTC:  KT1AfhJ6kxvtqYMRJ2uzq3xSiH9cvHBENGSV

tezosGoldExchange:   KT1NNHerEYbKvUMAhMYikRCs2Aoym7HQLqPB
tezosSilverExchange: KT1AecgjTUtHibSn64MbNQ4VBCNQELJweSZ4
tastyTacosExchange:  KT1Su9h3rAn6K66qucpN8954nrmjvBAoMmhp
checkersExchange:    KT1Rq4qVCkuDZQAE3YtoMQkk32dhMdZfwUcj
wrappedLTCExchange:  KT19Mteifa4XT4PtwRS5AWYMj5fV2sv6getq

init with AddLiquidity from alice:
tastyTacos:  500 XTZ, 100000 Tokens
tezosGold:   100 XTZ, 100000 Tokens
tezosSilver:  50 XTZ, 500000 Tokens

Checkers and Wrapped LTC
CHEK 1,000,000 WLTC 777,000
tyler's account: tz1SeGZ8JpFwSEYVAZo9NjjrLyPKyBwT8kRB

https://en.wikipedia.org/wiki/Relative_change_and_difference

Price impact for xtz to token

Mid price
The price between what users can buy and sell tokens at a given moment. In Uniswap this is the ratio of the two ERC20 token reserves.

Price impact
The difference between the mid-price and the execution price of a trade.


 Here is my interpretation of price impact for xtz to token. Take a look and let me know what you think. It is in python so you can play around with it.

execution price does not include the 0.3 liquidity fee
xtz pool is 100,000.0
(tzBTC) token pool is 10.0 

xtz_pool   = 100000.0
token_pool = 10.0
xtz_sold   = 500.00

```python
def price_impact(xtz_sold, xtz_pool, token_pool):
    mid_price = token_pool / xtz_pool
    tokens_bought = (xtz_sold * token_pool) / (xtz_sold + xtz_pool)
    execution_price = tokens_bought / xtz_sold
    price_impact = 1 - (execution_price / mid_price)
    price_impact2 = abs(mid_price-execution_price) / ((mid_price+execution_price) / 2.0)
    price_impact3 = abs(mid_price-execution_price) / mid_price
    price_impact3 = mid_price-execution_price
    print('mid_price:       {:.10f}'.format(mid_price))
    print('execution_price: {:.10f}'.format(execution_price))
    print('price_impact:    {:.10f}%').format(price_impact*100.0)
    print('price_impact2:   {:.10f}%').format(price_impact2*100.0)
    print('price_impact3:   {:.10f}%').format(price_impact3*100.0)


def price_impact(xtz_sold, xtz_pool, token_pool):
    mid_price = token_pool / xtz_pool
    tokens_bought = (xtz_sold * token_pool) / (xtz_sold + xtz_pool)
    execution_price = tokens_bought / xtz_sold
    price_impact = 1 - (execution_price / mid_price)
    print('mid_price:       {:.10f}'.format(mid_price))
    print('execution_price: {:.10f}'.format(execution_price))
    print('price_impact:    {:.10f}%').format(price_impact*100.0)

price_impact(0.1, 100000.0, 10.0)     # price_impact: 0.00009%
price_impact(1.0, 100000.0, 10.0)     # price_impact: 0.0009%
price_impact(10.0, 100000.0, 10.0)    # price_impact: 0.009%
price_impact(500.0, 100000.0, 10.0)   # price_impact: 0.49%
price_impact(1000.0, 100000.0, 10.0)  # price_impact: 0.99%
price_impact(50000.0, 100000.0, 10.0) # price_impact: 33.33%
price_impact(80000.0, 100000.0, 10.0) # price_impact: 44.44%
price_impact(95000.0, 100000.0, 10.0) # price_impact: 48.71%
```



```
/**
 * Returns the percent difference between the mid price and the execution price, i.e. price impact.
 * @param midPrice mid price before the trade
 * @param inputAmount the input amount of the trade
 * @param outputAmount the output amount of the trade
 */
function computePriceImpact(midPrice: Price, inputAmount: CurrencyAmount, outputAmount: CurrencyAmount): Percent {
  const exactQuote = midPrice.raw.multiply(inputAmount.raw)
  // calculate slippage := (exactQuote - outputAmount) / exactQuote
  const slippage = exactQuote.subtract(outputAmount.raw).divide(exactQuote)
  return new Percent(slippage.numerator, slippage.denominator)
}
```



```
function computePriceImpact(midPrice: Price, inputAmount: CurrencyAmount, outputAmount: CurrencyAmount): Percent {
  const exactQuote = midPrice.raw.multiply(inputAmount.raw)
  // calculate slippage := (exactQuote - outputAmount) / exactQuote
  const slippage = exactQuote.subtract(outputAmount.raw).divide(exactQuote)
  return new Percent(slippage.numerator, slippage.denominator)
}

xtz_pool   = 100000.0
token_pool = 10.0
xtz_sold   = 500.00

# based on uniswap
def price_impact(xtz_sold, xtz_pool, token_pool):
    mid_price = token_pool / xtz_pool
    tokens_bought = (xtz_sold * token_pool) / (xtz_sold + xtz_pool)
    exact_quote = mid_price * xtz_sold
    price_impact = (exact_quote - tokens_bought) / exact_quote
    print('mid_price:       {:.10f}'.format(mid_price))
    print('exact_quote: {:.10f}'.format(exact_quote))
    print('price_impact:    {:.10f}%'.format(price_impact*100.0))
    
price_impact(xtz_sold, xtz_pool, token_pool)
```




/**
 * Returns the percent difference between the mid price and the execution price, i.e. price impact.
 * @param midPrice mid price before the trade
 * @param inputAmount the input amount of the trade
 * @param outputAmount the output amount of the trade
 */
function computePriceImpact(midPrice: Price, inputAmount: CurrencyAmount, outputAmount: CurrencyAmount): Percent {
  const exactQuote = midPrice.raw.multiply(inputAmount.raw)
  // calculate slippage := (exactQuote - outputAmount) / exactQuote
  const slippage = exactQuote.subtract(outputAmount.raw).divide(exactQuote)
  return new Percent(slippage.numerator, slippage.denominator)
}

open Jest;
open Expect;

let expressions = Tezos_Expression.expressions;
let singleExpression = Tezos_Expression.singleExpression;

let readJson = filePath => {
  let json = Js.Json.parseExn(Node.Fs.readFileAsUtf8Sync(filePath));
  json;
};

let fp = "__tests__/golden/";

describe("decode", () => {
  test("transaction error", () => {
    expect(
      Tezos_TransactionError.decode(
        readJson(fp ++ "transaction_error.json"),
      ),
    )
    |> toEqual(
         Belt.Result.Ok(
           {
             kind: "temporary",
             id: "proto.006-PsCARTHA.michelson_v1.script_rejected",
             location: 7,
             with_:
               Tezos_Primitives.(
                 singleExpression(
                   ~prim=data(Tezos_PrimitiveData.Pair),
                   ~args=[
                     IntExpression(Bigint.of_string("1546544")),
                     singleExpression(
                       ~prim=data(Tezos_PrimitiveData.Unit),
                       (),
                     ),
                   ],
                   (),
                 )
               ),
           }: Tezos_TransactionError.t,
         ),
       )
  })
});

open Jest;
open Expect;

describe("ofString", () => {
  test("implicit account", () =>
    expect(Tezos.Address.ofString("tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"))
    |> toEqual(
         Belt.Result.Ok(
           Tezos_Address.ImplicitAccount(
             Tezos_ImplicitAccount.ImplicitAccount(
               "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn",
             ),
           ),
         ),
       )
  );

  test("contract", () =>
    expect(Tezos.Address.ofString("KT1Pu9Bopeq5CZu2WB83XVuhTJEJJxzf2VJo"))
    |> toEqual(
         Belt.Result.Ok(
           Tezos_Address.Contract(
             Tezos_Contract.Contract("KT1Pu9Bopeq5CZu2WB83XVuhTJEJJxzf2VJo"),
           ),
         ),
       )
  );

  test("implicit account with bad prefix", () =>
    expect(
      Tezos.Address.ofString("tz8boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn")
      |> Belt.Result.isError,
    )
    |> toEqual(true)
  );

  test("contract with bad prefix", () =>
    expect(
      Tezos.Address.ofString("KT9Pu9Bopeq5CZu2WB83XVuhTJEJJxzf2VJo")
      |> Belt.Result.isError,
    )
    |> toEqual(true)
  );

  test("implicit account with non bs58 characters", () =>
    expect(
      Tezos.Address.ofString("tz1boB5GrcAVRtjQQLrpjM4Xpp1E438L*&O0")
      |> Belt.Result.isError,
    )
    |> toEqual(true)
  );

  test("contract with non bs58 characters", () =>
    expect(
      Tezos.Address.ofString("KT1Pu9Bopeq5CZu2WB83XVuhTJEJJxzf1!~)")
      |> Belt.Result.isError,
    )
    |> toEqual(true)
  );

  test("implicit account too short", () =>
    expect(
      Tezos.Address.ofString("tz1boB5GrcAVRtjQQLrpj")
      |> Belt.Result.isError,
    )
    |> toEqual(true)
  );

  test("contract with too short", () =>
    expect(
      Tezos.Address.ofString("KT1Pu9Bopeq5CZu2WB83X")
      |> Belt.Result.isError,
    )
    |> toEqual(true)
  );  
});

describe("toScriptExpr", () => {
  test("implicit account", () =>
    expect(
      Tezos.Address.toScriptExpr(
        Common.unwrapResult(
          Tezos.Address.ofString("tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"),
        ),
      ),
    )
    |> toEqual("expruVJXegzJ1h5Q7wU9rXBVyXS1Sqj6No6tfqck3NNZmRrKmFQB1b")
  );

  test("contract", () =>
    expect(
      Tezos.Contract.toScriptExpr(
        Common.unwrapResult(
          Tezos.Contract.ofString("KT1Pu9Bopeq5CZu2WB83XVuhTJEJJxzf2VJo"),
        ),
      ),
    )
    |> toEqual("expruqtGt6To1vTMXRNTrfu1TgnyZH2a96c2jrj3Ws9UBA5qomvVeo")
  );
});

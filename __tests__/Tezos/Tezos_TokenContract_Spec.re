open Jest;
open Expect;

let expressions = Tezos_Expression.expressions;
let singleExpression = Tezos_Expression.singleExpression;

let readJson = filePath => {
  let json = Js.Json.parseExn(Node.Fs.readFileAsUtf8Sync(filePath));
  json;
};

let writeJson = (filePath, json) => {
  Node.Fs.writeFileAsUtf8Sync(filePath, json);
};

let fp = "__tests__/golden/";

let pushMutez =
  Tezos_Primitives.(
    expressions([
      singleExpression(
        ~prim=instr(Tezos_PrimitiveInstruction.Push),
        ~args=[
          singleExpression(~prim=type_(Tezos_PrimitiveType.Mutez), ()),
          IntExpression(Bigint.zero),
        ],
        (),
      ),
    ])
  );

let createContract =
  Tezos_Primitives.(
    expressions([
      singleExpression(
        ~prim=instr(Tezos_PrimitiveInstruction.CreateContract),
        ~args=[
          expressions([
            singleExpression(
              ~prim=type_(Tezos_PrimitiveType.Parameter),
              ~args=[
                singleExpression(~prim=type_(Tezos_PrimitiveType.Nat), ()),
              ],
              (),
            ),
            singleExpression(
              ~prim=type_(Tezos_PrimitiveType.Storage),
              ~args=[
                singleExpression(~prim=type_(Tezos_PrimitiveType.Unit), ()),
              ],
              (),
            ),
            singleExpression(
              ~prim=type_(Tezos_PrimitiveType.Code),
              ~args=[
                expressions([
                  singleExpression(
                    ~prim=instr(Tezos_PrimitiveInstruction.Failwith),
                    (),
                  ),
                ]),
              ],
              (),
            ),
          ]),
        ],
        (),
      ),
    ])
  );

let dip =
  Tezos_Primitives.(
    expressions([
      singleExpression(
        ~prim=instr(Tezos_PrimitiveInstruction.Dip),
        ~args=[
          expressions([
            singleExpression(
              ~prim=instr(Tezos_PrimitiveInstruction.Nil),
              ~args=[
                singleExpression(
                  ~prim=type_(Tezos_PrimitiveType.Operation),
                  (),
                ),
              ],
              (),
            ),
          ]),
        ],
        (),
      ),
    ])
  );

let lambda =
  Tezos_Primitives.(
    expressions([
      singleExpression(
        ~prim=instr(Tezos_PrimitiveInstruction.Lambda),
        ~args=[
          singleExpression(
            ~prim=type_(Tezos_PrimitiveType.Pair),
            ~args=[
              singleExpression(~prim=type_(Tezos_PrimitiveType.Address), ()),
              singleExpression(~prim=type_(Tezos_PrimitiveType.Unit), ()),
            ],
            (),
          ),
          singleExpression(
            ~prim=type_(Tezos_PrimitiveType.Pair),
            ~args=[
              singleExpression(
                ~prim=type_(Tezos_PrimitiveType.List),
                ~args=[
                  singleExpression(
                    ~prim=type_(Tezos_PrimitiveType.Operation),
                    (),
                  ),
                ],
                (),
              ),
              singleExpression(~prim=type_(Tezos_PrimitiveType.Unit), ()),
            ],
            (),
          ),
          expressions([
            singleExpression(
              ~prim=instr(Tezos_PrimitiveInstruction.Car),
              (),
            ),
          ]),
        ],
        (),
      ),
    ])
  );

let ifNone =
  Tezos_Primitives.(
    expressions([
      singleExpression(
        ~prim=instr(Tezos_PrimitiveInstruction.IfNone),
        ~args=[
          expressions([
            singleExpression(
              ~prim=instr(Tezos_PrimitiveInstruction.Push),
              ~args=[
                singleExpression(
                  ~prim=type_(Tezos_PrimitiveType.String),
                  (),
                ),
                StringExpression("a"),
              ],
              (),
            ),
            singleExpression(
              ~prim=instr(Tezos_PrimitiveInstruction.Failwith),
              (),
            ),
          ]),
          expressions([]),
        ],
        (),
      ),
    ])
  );

describe("building code expressions to be used in view contract", () => {
  test("PUSH mutez 0", () => {
    expect(Tezos_Expression.encode(pushMutez))
    |> toEqual(readJson(fp ++ "expression_mutez.json"))
  });

  test(
    "CREATE_CONTRACT { parameter nat ; storage unit ; code { FAILWITH } }", () => {
    expect(Tezos_Expression.encode(createContract))
    |> toEqual(readJson(fp ++ "expression_create_contract.json"))
  });

  test("DIP { NIL operation }", () => {
    expect(Tezos_Expression.encode(dip))
    |> toEqual(readJson(fp ++ "expression_dip.json"))
  });

  test("LAMBDA (pair address unit) (pair (list operation) unit)", () => {
    expect(Tezos_Expression.encode(lambda))
    |> toEqual(readJson(fp ++ "expression_lambda.json"))
  });

  test("IF_NONE {PUSH string \"a\"; FAILWTIH; } {}", () => {
    expect(Tezos_Expression.encode(ifNone))
    |> toEqual(readJson(fp ++ "expression_ifnone.json"))
  });

  test("Query generic view contract to FA1.2 getAllowanceExpression", () => {
    expect(
      Tezos_Expression.encode(
        Tezos_ContractView.getAllowanceExpression(
          ~sourceContract="KT1U8kHUKJVcPkzjHLahLxmnXGnK1StAeNjK",
          ~viewContract="KT1Njyz94x2pNJGh5uMhKj24VB9JsGCdkySN",
          ~owner="tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z",
          ~sender="tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z",
          (),
        ),
      ),
    )
    |> toEqual(readJson(fp ++ "view_fa12_get_allowance.json"))
  });

  test("Query generic view contract to FA1.2 getBalance", () => {
    expect(
      Tezos_Expression.encode(
        Tezos_ContractView.getBalance(
          ~sourceContract="KT1U8kHUKJVcPkzjHLahLxmnXGnK1StAeNjK",
          ~viewContract="KT1Njyz94x2pNJGh5uMhKj24VB9JsGCdkySN",
          ~owner="tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z",
          (),
        ),
      ),
    )
    |> toEqual(readJson(fp ++ "view_fa12_get_balance.json"))
  });

  test("Query generic view contract to FA1.2 getTotalSupply", () => {
    expect(
      Tezos_Expression.encode(
        Tezos_ContractView.getTotalSupply(
          ~sourceContract="KT1U8kHUKJVcPkzjHLahLxmnXGnK1StAeNjK",
          ~viewContract="KT1Njyz94x2pNJGh5uMhKj24VB9JsGCdkySN",
          (),
        ),
      ),
    )
    |> toEqual(readJson(fp ++ "view_fa12_get_total_supply.json"))
  });

  test("dry run", () => {
    expect(
      Tezos_DryRun.encodeDryRunTransaction(
        Tezos_ContractView.getAllowancePayload(
          ~counter=Bigint.of_string("552906"),
          ~block="BM5qVBbTfXjRDo9DDmvmUnKc4hMM3i6BpwNb8AwmwowE5a8V7Yb",
          ~chainId="NetXjD3HPJJjmcd",
          ~source="tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV",
          ~sourceContract="KT1U8kHUKJVcPkzjHLahLxmnXGnK1StAeNjK",
          ~viewContract="KT1Njyz94x2pNJGh5uMhKj24VB9JsGCdkySN",
          ~owner="tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z",
          ~sender="tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z",
          (),
        ),
      ),
    )
    |> toEqual(readJson(fp ++ "view_dry_run.json"))
  });
});

open Jest;
open Expect;

module T = Tezos_Token;
let m = T.mkToken;

describe("Tezos_Token.ofString", () => {
  test("'0'", _ => {
    expect(T.ofString("0", 0))
    |> toEqual(Belt.Result.Ok(m(Bigint.zero, 0)))
  });

  test("'1'", _ => {
    expect(T.ofString("1", 1))
    |> toEqual(Belt.Result.Ok(m(Bigint.of_int(10), 1)))
  });

  test("'1.0'", _ => {
    expect(T.ofString("1.0", 1))
    |> toEqual(Belt.Result.Ok(m(Bigint.of_int(10), 1)))
  });

  test("'1.01'", _ => {
    expect(T.ofString("1.01", 2))
    |> toEqual(Belt.Result.Ok(m(Bigint.of_int(101), 2)))
  });

  test("'1.023'", _ => {
    expect(T.ofString("1.023", 3))
    |> toEqual(Belt.Result.Ok(m(Bigint.of_int(1023), 3)))
  });

  test("'123.456789'", _ => {
    expect(T.ofString("123.456789", 6))
    |> toEqual(Belt.Result.Ok(m(Bigint.of_int(123456789), 6)))
  });

  test("'0.0089'", _ => {
    expect(T.ofString("00.0089", 4))
    |> toEqual(Belt.Result.Ok(m(Bigint.of_int(89), 4)))
  });

  test("'984'", _ => {
    expect(T.ofString("984", 6))
    |> toEqual(Belt.Result.Ok(m(Bigint.of_int(984000000), 6)))
  });
});

describe("Tezos_Token.toString", () => {
  test("0 -> 0", _ => {
    expect(T.toString(m(Bigint.zero, 0))) |> toEqual("0")
  });

  test("0.0 -> 0", _ => {
    expect(T.toString(m(Bigint.zero, 1))) |> toEqual("0")
  });

  test("1.0205", _ => {
    expect(T.toString(m(Bigint.of_int(10205), 4))) |> toEqual("1.0205")
  });

  test("1", _ => {
    expect(T.toString(m(Bigint.of_int(1000000), 6))) |> toEqual("1")
  });

  test("67.00402", _ => {
    expect(T.toString(m(Bigint.of_int(67004020), 6)))
    |> toEqual("67.00402")
  });
});

describe("Tezos_Token.toStringWithCommas", () => {
  test("0", _ => {
    expect(T.toStringWithCommas(m(Bigint.zero, 0))) |> toEqual("0")
  });

  test("1", _ => {
    expect(T.toStringWithCommas(m(Bigint.of_int(1000000), 6)))
    |> toEqual("1")
  });

  test("67.00402", _ => {
    expect(T.toStringWithCommas(m(Bigint.of_int(67004020), 6)))
    |> toEqual("67.00402")
  });

  test("1,267.00402", _ => {
    expect(T.toStringWithCommas(m(Bigint.of_int(1267004020), 6)))
    |> toEqual("1,267.00402")
  });

  test("45,657,923.00402", _ => {
    expect(T.toStringWithCommas(m(Bigint.of_string("45657923004020"), 6)))
    |> toEqual("45,657,923.00402")
  });
});

describe("Tezos_Token.compare", () => {
  test("compare(0,1)", _ => {
    expect(T.compare(m(Bigint.zero, 0), m(Bigint.one, 0))) |> toEqual(-1)
  });

  test("compare(1,0)", _ => {
    expect(T.compare(m(Bigint.one, 0), m(Bigint.zero, 0))) |> toEqual(1)
  });

  test("compare(0,0)", _ => {
    expect(T.compare(m(Bigint.zero, 0), m(Bigint.zero, 0))) |> toEqual(0)
  });

  test("compare(1.0,0)", _ => {
    expect(T.compare(m(Bigint.of_int(10), 1), m(Bigint.zero, 0)))
    |> toEqual(1)
  });

  test("compare(1.02,2.03)", _ => {
    expect(T.compare(m(Bigint.of_int(102), 2), m(Bigint.of_int(203), 2)))
    |> toEqual(-1)
  });

  test("compare(1.02,0.2)", _ => {
    expect(T.compare(m(Bigint.of_int(102), 1), m(Bigint.of_int(2), 1)))
    |> toEqual(1)
  });
});

describe("Tezos_Token.subPointThreePercent", _ => {
  test("1", _ => {
    expect(T.subPointThreePercent(m(Bigint.of_int(1), 0)))
    |> toEqual(m(Bigint.of_int(0), 0))
  });

  test("2", _ => {
    expect(T.subPointThreePercent(m(Bigint.of_int(2), 0)))
    |> toEqual(m(Bigint.of_int(1), 0))
  });

  test("1000", _ => {
    expect(T.subPointThreePercent(m(Bigint.of_int(1000), 0)))
    |> toEqual(m(Bigint.of_int(997), 0))
  });

  test("2000", _ => {
    expect(T.subPointThreePercent(m(Bigint.of_int(2000), 0)))
    |> toEqual(m(Bigint.of_int(1994), 0))
  });

  test("10000", _ => {
    expect(T.subPointThreePercent(m(Bigint.of_int(10000), 0)))
    |> toEqual(m(Bigint.of_int(9970), 0))
  });

  test("22345", _ => {
    expect(T.subPointThreePercent(m(Bigint.of_int(22345), 0)))
    |> toEqual(m(Bigint.of_int(22277), 0))
  });

  test("1.000", _ => {
    expect(T.subPointThreePercent(m(Bigint.of_int(1000), 3)))
    |> toEqual(m(Bigint.of_int(997), 3))
  });
});

describe("Tezos_Token.addPointThreePercent", _ => {
  test("1", _ => {
    expect(T.addPointThreePercent(m(Bigint.of_int(1), 0)))
    |> toEqual(m(Bigint.of_int(1), 0))
  });

  test("2", _ => {
    expect(T.addPointThreePercent(m(Bigint.of_int(2), 0)))
    |> toEqual(m(Bigint.of_int(2), 0))
  });

  test("1000", _ => {
    expect(T.addPointThreePercent(m(Bigint.of_int(1000), 0)))
    |> toEqual(m(Bigint.of_int(1003), 0))
  });

  test("2000", _ => {
    expect(T.addPointThreePercent(m(Bigint.of_int(2000), 0)))
    |> toEqual(m(Bigint.of_int(2006), 0))
  });

  test("10000", _ => {
    expect(T.addPointThreePercent(m(Bigint.of_int(10000), 0)))
    |> toEqual(m(Bigint.of_int(10030), 0))
  });

  test("22345", _ => {
    expect(T.addPointThreePercent(m(Bigint.of_int(22345), 0)))
    |> toEqual(m(Bigint.of_int(22412), 0))
  });

  test("1.000", _ => {
    expect(T.addPointThreePercent(m(Bigint.of_int(1000), 3)))
    |> toEqual(m(Bigint.of_int(1003), 3))
  });
});

describe("Tezos_Token.toFloat", () => {
  test("1.23", _ => {
    expect(Belt.Result.map(T.ofString("1.23", 2), T.toFloatWithDecimal))
    |> toEqual(Belt.Result.Ok(1.23))
  });

  test("0.001", _ => {
    expect(Belt.Result.map(T.ofString("0.001", 3), T.toFloatWithDecimal))
    |> toEqual(Belt.Result.Ok(0.001))
  });

  test("1.0", _ => {
    expect(Belt.Result.map(T.ofString("1.0", 4), T.toFloatWithDecimal))
    |> toEqual(Belt.Result.Ok(1.0))
  });

  test("104.112233", _ => {
    expect(
      Belt.Result.map(T.ofString("104.112233", 6), T.toFloatWithDecimal),
    )
    |> toEqual(Belt.Result.Ok(104.112233))
  });
});

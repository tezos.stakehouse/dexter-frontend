open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("TZG Exchange Balance; Light mode;", () => {
    render(
      <Helpers_Context>
        <DexterUi_TransactionsPoolTokensRow
          token=Fixtures_Balances.tzgExchangeBalance
        />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("MTZ Exchange Balance; Dark mode;", () => {
    render(
      <Helpers_Context darkMode=true>
        <DexterUi_TransactionsPoolTokensRow
          token=Fixtures_Balances.mtzExchangeBalance
        />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Render", () => {
    render(<DexterUi_WalletConnect />)
    |> container
    |> expect
    |> toMatchSnapshot
  })
});

open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("No account", () => {
    render(<Helpers_Context> <DexterUi_Wallet /> </Helpers_Context>)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("With account", () => {
    render(
      <Helpers_Context account=Fixtures_Account.account1>
        <DexterUi_Wallet />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

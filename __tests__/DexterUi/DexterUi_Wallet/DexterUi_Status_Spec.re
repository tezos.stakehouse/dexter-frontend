open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Exchange ; Dark mode; Failed", () => {
    render(
      <Helpers_Context
        account=Fixtures_Account.account1
        transactions=[Fixtures_Transactions.exchange]
        transactionStatus={Failed(Unknown)}
        darkMode=true>
        <DexterUi_Status />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Exchange; Dark mode; Failed Unknown", () => {
    render(
      <Helpers_Context
        account=Fixtures_Account.account1
        transactions=[Fixtures_Transactions.exchange]
        transactionStatus={Failed(Unknown)}
        darkMode=true>
        <DexterUi_Status />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Exchange; Dark mode; Failed SlippageLimitExceeded", () => {
    render(
      <Helpers_Context
        account=Fixtures_Account.account1
        transactions=[Fixtures_Transactions.exchange]
        transactionStatus={Failed(SlippageLimitExceeded)}
        darkMode=true>
        <DexterUi_Status />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Exchange; Dark mode; Applied", () => {
    render(
      <Helpers_Context
        account=Fixtures_Account.account1
        transactions=[Fixtures_Transactions.exchange]
        transactionStatus=Applied
        darkMode=true>
        <DexterUi_Status />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Exchange; Light mode; Pending", () => {
    render(
      <Helpers_Context
        account=Fixtures_Account.account1
        transactions=[Fixtures_Transactions.exchange]
        transactionStatus=Pending>
        <DexterUi_Status />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Exchange and send; Dark mode; Applied", () => {
    render(
      <Helpers_Context
        account=Fixtures_Account.account1
        transactions=[Fixtures_Transactions.exchangeAndSend]
        transactionStatus=Applied
        darkMode=true>
        <DexterUi_Status />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Add liquidity; Light mode; Applied", () => {
    render(
      <Helpers_Context
        account=Fixtures_Account.account1
        transactions=[Fixtures_Transactions.addLiquidity]
        transactionStatus=Applied>
        <DexterUi_Status />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Remove liquidity; Light mode; Failed", () => {
    render(
      <Helpers_Context
        account=Fixtures_Account.account1
        transactions=[Fixtures_Transactions.removeLiquidity]
        transactionStatus={Failed(Unknown)}>
        <DexterUi_Status />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

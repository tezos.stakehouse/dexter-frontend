open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("No account", () => {
    render(
      <Helpers_Context>
        <DexterUi_LiquidityExplainerInfoRow
          boxHeight={`px(118)}
          number=1
          image="/img/liquidity-explainer-1.png">
          {"Snapshot" |> React.string}
        </DexterUi_LiquidityExplainerInfoRow>
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("With account", () => {
    render(
      <Helpers_Context account=Fixtures_Account.account1>
        <DexterUi_LiquidityExplainerInfoRow
          boxHeight={`px(118)}
          number=1
          image="/img/liquidity-explainer-1.png">
          {"Snapshot" |> React.string}
        </DexterUi_LiquidityExplainerInfoRow>
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("TZG", () => {
    render(
      <Helpers_Context account=Fixtures_Account.account1>
        <DexterUi_PoolDataRowButtons
          token=Fixtures_Balances.tzgExchangeBalance
        />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("MTZ", () => {
    render(
      <Helpers_Context account=Fixtures_Account.account1>
        <DexterUi_PoolDataRowButtons
          token=Fixtures_Balances.mtzExchangeBalance
        />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("MTZ; No XTZ", () => {
    render(
      <Helpers_Context
        account={...Fixtures_Account.account1, xtz: Tezos.Mutez.zero}>
        <DexterUi_PoolDataRowButtons
          token=Fixtures_Balances.mtzExchangeBalance
        />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Enabled", () => {
    render(
      <DexterUi_PoolDataRowButton
        hash="snapshot"
        route=Dexter_Route.Exchange
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Disabled", () => {
    render(
      <DexterUi_PoolDataRowButton
        disabled=true
        hash="snapshot"
        route=Dexter_Route.Exchange
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("TZG", () => {
    render(
      <DexterUi_PoolDataRow token=Fixtures_Balances.tzgExchangeBalance />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("MTZ", () => {
    render(
      <DexterUi_PoolDataRow token=Fixtures_Balances.mtzExchangeBalance />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

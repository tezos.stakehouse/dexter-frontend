open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Light mode; Primary; Horizontal padding 10px", () => {
    render(
      <Helpers_Context>
        <DexterUi_Button px={`px(10)}>
          {"Snapshot" |> React.string}
        </DexterUi_Button>
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Light mode; Secondary; Disabled; Horizontal padding 10px", () => {
    render(
      <Helpers_Context>
        <DexterUi_Button px={`px(10)} variant=Secondary disabled=true>
          {"Snapshot" |> React.string}
        </DexterUi_Button>
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode; Primary; Small; Width 120px", () => {
    render(
      <Helpers_Context darkMode=true>
        <DexterUi_Button width={`px(120)} small=true>
          {"Snapshot" |> React.string}
        </DexterUi_Button>
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode; Primary; Dark mode disabled; Width 120px", () => {
    render(
      <Helpers_Context darkMode=true>
        <DexterUi_Button width={`px(120)} darkModeEnabled=false>
          {"Snapshot" |> React.string}
        </DexterUi_Button>
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

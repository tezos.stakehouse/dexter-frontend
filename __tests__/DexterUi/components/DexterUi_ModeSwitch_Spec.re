open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Light mode", () => {
    render(<Helpers_Context> <DexterUi_ModeSwitch /> </Helpers_Context>)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode", () => {
    render(
      <Helpers_Context darkMode=true>
        <DexterUi_ModeSwitch />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

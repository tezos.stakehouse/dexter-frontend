open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Primary; Inactive", () => {
    render(
      <DexterUi_Tab
        onClick={_ => ()}
        tabText="Liquidity Pool"
        iconName="pool-tab"
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Secondary; Active", () => {
    render(
      <DexterUi_Tab
        isActive=true
        onClick={_ => ()}
        tabText="Liquidity Pool"
        iconName="pool-tab"
        variant=Secondary
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("TZG; currencies empty", () => {
    render(
      <Helpers_Context currencies=[]>
        <DexterUi_CurrencyValue
          token=Fixtures_Balances.tzgExchangeBalance
          value={Token(Tezos.Token.mkToken(Bigint.of_int64(1000L), 0))}
        />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("TZG; currency USD", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyUSD>
        <DexterUi_CurrencyValue
          token=Fixtures_Balances.tzgExchangeBalance
          value={Token(Tezos.Token.mkToken(Bigint.of_int64(1000L), 0))}
        />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("TZG; currency BTC", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyBTC>
        <DexterUi_CurrencyValue
          token=Fixtures_Balances.tzgExchangeBalance
          value={Token(Tezos.Token.mkToken(Bigint.of_int64(1000L), 0))}
        />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("TZG; currencies error", () => {
    render(
      <Helpers_Context unavailableServices=[|Service.CoinGecko|]>
        <DexterUi_CurrencyValue
          token=Fixtures_Balances.tzgExchangeBalance
          value={Token(Tezos.Token.mkToken(Bigint.of_int64(1000L), 0))}
        />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("XTZ; currency USD", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyUSD>
        <DexterUi_CurrencyValue value={Mutez(Mutez(10000000L))} />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("XTZ; currency BTC", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyBTC>
        <DexterUi_CurrencyValue value={Mutez(Mutez(10000000L))} />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

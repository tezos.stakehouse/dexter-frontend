open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("XTZ/MTZ", () => {
    render(
      <DexterUi_PoolTokenSymbol token=Fixtures_Balances.mtzExchangeBalance />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("XTZ/TZG", () => {
    render(
      <DexterUi_PoolTokenSymbol token=Fixtures_Balances.tzgExchangeBalance />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Normal XTZ/tzBTC", () => {
    render(<DexterUi_PoolTokenIcon icon="/img/tzBTC-logo.png" />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Small XTZ/USDtz", () => {
    render(<DexterUi_PoolTokenIcon icon="/img/USDtz-logo.png" small=true />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

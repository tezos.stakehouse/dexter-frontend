open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Simple", () => {
    render(<DexterUi_Box> {"Snapshot" |> React.string} </DexterUi_Box>)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("With tabs", () => {
    render(
      <DexterUi_Box tabs={"Snapshot tabs" |> React.string}>
        {"Snapshot" |> React.string}
      </DexterUi_Box>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

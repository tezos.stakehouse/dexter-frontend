open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Gray; Light mode", () => {
    render(
      <Helpers_Context>
        <DexterUi_Panel> {"Snapshot" |> React.string} </DexterUi_Panel>
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Gray; Dark mode", () => {
    render(
      <Helpers_Context darkMode=true>
        <DexterUi_Panel> {"Snapshot" |> React.string} </DexterUi_Panel>
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Blue; Light mode", () => {
    render(
      <Helpers_Context>
        <DexterUi_Panel variant=Blue>
          {"Snapshot" |> React.string}
        </DexterUi_Panel>
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Blue; Dark mode", () => {
    render(
      <Helpers_Context darkMode=true>
        <DexterUi_Panel variant=Blue>
          {"Snapshot" |> React.string}
        </DexterUi_Panel>
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Orange; Light mode", () => {
    render(
      <Helpers_Context>
        <DexterUi_Panel variant=Orange>
          {"Snapshot" |> React.string}
        </DexterUi_Panel>
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Orange; Dark mode", () => {
    render(
      <Helpers_Context darkMode=true>
        <DexterUi_Panel variant=Orange>
          {"Snapshot" |> React.string}
        </DexterUi_Panel>
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Red; Light mode", () => {
    render(
      <Helpers_Context>
        <DexterUi_Panel variant=Red>
          {"Snapshot" |> React.string}
        </DexterUi_Panel>
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Red; Dark mode", () => {
    render(
      <Helpers_Context darkMode=true>
        <DexterUi_Panel variant=Red>
          {"Snapshot" |> React.string}
        </DexterUi_Panel>
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

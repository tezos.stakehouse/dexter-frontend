open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Just title", () => {
    render(<DexterUi_Message title={"Snapshot title" |> React.string} />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Title and subtitle", () => {
    render(
      <DexterUi_Message
        title={"Snapshot title" |> React.string}
        subtitle={"Snapshot subtitle" |> React.string}
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Title and subtitle; Dark mode disabled", () => {
    render(
      <DexterUi_Message
        darkModeEnabled=false
        title={"Snapshot title" |> React.string}
        subtitle={"Snapshot subtitle" |> React.string}
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

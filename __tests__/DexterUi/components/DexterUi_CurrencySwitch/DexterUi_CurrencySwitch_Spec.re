open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("currencies empty", () => {
    render(
      <Helpers_Context currencies=[]>
        <DexterUi_CurrencySwitch />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currency USD", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyUSD>
        <DexterUi_CurrencySwitch />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currency BTC", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyBTC>
        <DexterUi_CurrencySwitch />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currencies error", () => {
    render(
      <Helpers_Context unavailableServices=[|Service.CoinGecko|]>
        <DexterUi_CurrencySwitch />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("currentCurrency USD; currency USD", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyUSD>
        <DexterUi_CurrencySwitchListOption
          currency=Fixtures_Currencies.currencyUSD
        />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currentCurrency BTC; currency USD", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyBTC>
        <DexterUi_CurrencySwitchListOption
          currency=Fixtures_Currencies.currencyUSD
        />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currentCurrency BTC; currency BTC", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyBTC>
        <DexterUi_CurrencySwitchListOption
          currency=Fixtures_Currencies.currencyBTC
        />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currentCurrency USD; currency JPY", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyUSD>
        <DexterUi_CurrencySwitchListOption
          currency=Fixtures_Currencies.currencyJPY
        />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

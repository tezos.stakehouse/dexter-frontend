open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("currency USD", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyUSD>
        <DexterUi_CurrencySwitchList onClose={() => ()} />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currency BTC", () => {
    render(
      <Helpers_Context currentCurrency=Fixtures_Currencies.currencyBTC>
        <DexterUi_CurrencySwitchList onClose={() => ()} />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currencies error", () => {
    render(
      <Helpers_Context unavailableServices=[|Service.CoinGecko|]>
        <DexterUi_CurrencySwitchList onClose={() => ()} />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});

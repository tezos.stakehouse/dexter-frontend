open Jest;
open Expect;
open Dexter_Exchange;

// calculate how many XTZ a single token is worth at the market value.
describe("xtzToTokenMarketRate", () => {
  test("1 XTZ to 15,000,000 Token", () => {
    let result =
      xtzToTokenMarketRateForDisplay(
        Tezos.Mutez.Mutez(1000000L),
        Tezos.Token.mkToken(Bigint.of_int64(15000000L), 0),
      );

    expect(result) |> toEqual(15000000.0);
  });

  test("10 XTZ to 10 Token", () => {
    let result =
      xtzToTokenMarketRateForDisplay(
        Tezos.Mutez.Mutez(10000000L),
        Tezos.Token.mkToken(Bigint.of_int64(10L), 0),
      );

    expect(result) |> toEqual(1.0);
  });

  test("100 XTZ to 2.5 Token", () => {
    let result =
      xtzToTokenMarketRateForDisplay(
        Tezos.Mutez.Mutez(100000000L),
        Tezos.Token.mkToken(Bigint.of_int64(25L), 1),
      );

    expect(result) |> toEqual(0.025);
  });
});

// calculate how many tokens a single XTZ is worth at the market value.
describe("tokenToXtzMarketRate", () => {
  test("1 XTZ to 15,000,000 Token", () => {
    let result =
      tokenToXtzMarketRateForDisplay(
        Tezos.Mutez.Mutez(1000000L),
        Tezos.Token.mkToken(Bigint.of_int64(15000000L), 0),
      );

    expect(result) |> toEqual(0.00000006666666666666667);
  })
});

let fp = "__tests__/golden/";

module TokenToXtzTest = {
  type t = {
    xtzPool: Tezos.Mutez.t,
    tokenPool: Bigint.t,
    tokenIn: Bigint.t,
    xtzOut: Tezos.Mutez.t,
  };

  let decode = (json: Js.Json.t) =>
    switch (
      Json.Decode.{
        xtzPool:
          Tezos_Util.unwrapResult(
            field("xtz_pool", Tezos.Mutez.decode, json),
          ),
        tokenPool:
          Tezos_Util.unwrapResult(
            field("token_pool", Tezos_Util.bigintDecode, json),
          ),
        tokenIn:
          Tezos_Util.unwrapResult(
            field("token_in", Tezos_Util.bigintDecode, json),
          ),
        xtzOut:
          Tezos_Util.unwrapResult(
            field("xtz_out", Tezos.Mutez.decode, json),
          ),
      }
    ) {
    | v => Belt.Result.Ok(v)
    | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err)
    };
};

describe("token to xtz golden tests", () => {
  let goldenFile =
    Js.Json.parseExn(Node.Fs.readFileAsUtf8Sync(fp ++ "token_to_xtz.json"));
  let goldenTests = Json.Decode.list(TokenToXtzTest.decode, goldenFile);

  List.map(
    (goldenTest: Belt.Result.t(TokenToXtzTest.t, string)) => {
      switch (goldenTest) {
      | Belt.Result.Ok(goldenTest) =>
        let tokenIn = Tezos.Token.mkToken(goldenTest.tokenIn, 0);
        let tokenPool = Tezos.Token.mkToken(goldenTest.tokenPool, 0);
        test(
          "getXtzToTokenToRate: token_in: "
          ++ Tezos.Token.toString(tokenIn)
          ++ ", xtz_pool: "
          ++ Tezos.Mutez.toString(goldenTest.xtzPool)
          ++ ", token_pool: "
          ++ Tezos.Token.toString(tokenPool)
          ++ " expects xtz_out: "
          ++ Tezos.Mutez.toString(goldenTest.xtzOut),
          () => {
          expect(tokenToXtzXtzOutput(tokenIn, goldenTest.xtzPool, tokenPool))
          |> toEqual(Some(goldenTest.xtzOut))
        });
      | Belt.Result.Error(err) => test("unable to read JSON", () =>
                                    fail(err)
                                  )
      }
    },
    goldenTests,
  )
  |> ignore;
});

describe("tokenToXtzXtzOutput", () => {
  test("token_in: 1, " ++ "xtz_pool: 1000, " ++ "token_pool: 10000", () =>
    expect(
      tokenToXtzXtzOutput(
        Tezos.Token.mkToken(Bigint.of_int(1), 3),
        Tezos.Mutez.ofIntUnsafe(1000000000),
        Tezos.Token.mkToken(Bigint.of_int(10000), 3),
      ),
    )
    |> toEqual(Some(Tezos.Mutez.ofIntUnsafe(99690)))
  )
});

describe("tokenToXtzExchangeRate", () => {
  test("", () => {
    let tokenIn = Tezos.Token.mkToken(Bigint.of_int64(10000L), 0);
    let xtzPool = Tezos.Mutez.Mutez(2000000L);
    let tokenPool = Tezos.Token.mkToken(Bigint.of_int64(100000L), 0);

    let result = tokenToXtzExchangeRate(tokenIn, xtzPool, tokenPool);

    expect(result) |> toEqual(18.1322);
  });

  test("", () => {
    let tokenIn = Tezos.Token.mkToken(Bigint.of_int64(10000L), 2);
    let xtzPool = Tezos.Mutez.Mutez(2000000L);
    let tokenPool = Tezos.Token.mkToken(Bigint.of_int64(100000L), 2);

    let result = tokenToXtzExchangeRate(tokenIn, xtzPool, tokenPool);

    expect(result) |> toEqual(18.1322);
  });

  test("", () => {
    let tokenIn = Tezos.Token.mkToken(Bigint.of_int64(1L), 0);
    let xtzPool = Tezos.Mutez.Mutez(30000000000L);
    let tokenPool = Tezos.Token.mkToken(Bigint.of_int64(200L), 0);

    let result = tokenToXtzExchangeRate(tokenIn, xtzPool, tokenPool);

    expect(result) |> toEqual(148808191.0);
  });
});

describe("tokenToXtzExchangeRateForDisplay", () => {
  test("", () => {
    let tokenIn = Tezos.Token.mkToken(Bigint.of_int64(10000L), 0);
    let xtzPool = Tezos.Mutez.Mutez(2000000L);
    let tokenPool = Tezos.Token.mkToken(Bigint.of_int64(100000L), 0);

    let result =
      tokenToXtzExchangeRateForDisplay(tokenIn, xtzPool, tokenPool);

    expect(result) |> toEqual(0.0000181322);
  });

  test("", () => {
    let tokenIn = Tezos.Token.mkToken(Bigint.of_int64(10000L), 2);
    let xtzPool = Tezos.Mutez.Mutez(2000000L);
    let tokenPool = Tezos.Token.mkToken(Bigint.of_int64(100000L), 2);

    let result =
      tokenToXtzExchangeRateForDisplay(tokenIn, xtzPool, tokenPool);

    expect(result) |> toEqual(0.00181322);
  });

  test("", () => {
    let tokenIn = Tezos.Token.mkToken(Bigint.of_int64(1L), 0);
    let xtzPool = Tezos.Mutez.Mutez(30000000000L);
    let tokenPool = Tezos.Token.mkToken(Bigint.of_int64(200L), 0);

    let result =
      tokenToXtzExchangeRateForDisplay(tokenIn, xtzPool, tokenPool);

    expect(result) |> toEqual(148.808191);
  });
});

module XtzToTokenTest = {
  type t = {
    xtzPool: Tezos.Mutez.t,
    tokenPool: Bigint.t,
    xtzIn: Tezos.Mutez.t,
    tokenOut: Bigint.t,
  };

  let decode = (json: Js.Json.t) =>
    switch (
      Json.Decode.{
        xtzPool:
          Tezos_Util.unwrapResult(
            field("xtz_pool", Tezos.Mutez.decode, json),
          ),
        tokenPool:
          Tezos_Util.unwrapResult(
            field("token_pool", Tezos_Util.bigintDecode, json),
          ),
        xtzIn:
          Tezos_Util.unwrapResult(field("xtz_in", Tezos.Mutez.decode, json)),
        tokenOut:
          Tezos_Util.unwrapResult(
            field("token_out", Tezos_Util.bigintDecode, json),
          ),
      }
    ) {
    | v => Belt.Result.Ok(v)
    | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err)
    };
};

describe("xtz to token golden tests", () => {
  let goldenFile =
    Js.Json.parseExn(Node.Fs.readFileAsUtf8Sync(fp ++ "xtz_to_token.json"));
  let goldenTests = Json.Decode.list(XtzToTokenTest.decode, goldenFile);

  List.map(
    (goldenTest: Belt.Result.t(XtzToTokenTest.t, string)) => {
      switch (goldenTest) {
      | Belt.Result.Ok(goldenTest) =>
        let tokenPool = Tezos.Token.mkToken(goldenTest.tokenPool, 0);
        let tokenOut = Tezos.Token.mkToken(goldenTest.tokenOut, 0);
        test(
          "xtzToToken: xtz_in: "
          ++ Tezos.Mutez.toString(goldenTest.xtzIn)
          ++ ", xtz_pool: "
          ++ Tezos.Mutez.toString(goldenTest.xtzPool)
          ++ ", token_pool: "
          ++ Tezos.Token.toString(tokenPool)
          ++ " expects token_out: "
          ++ Tezos.Token.toString(tokenOut),
          () => {
          expect(
            xtzToTokenTokenOutput(
              goldenTest.xtzIn,
              goldenTest.xtzPool,
              tokenPool,
            ),
          )
          |> toEqual(Some(tokenOut))
        });

      | Belt.Result.Error(err) => test("unable to read JSON", () =>
                                    fail(err)
                                  )
      }
    },
    goldenTests,
  )
  |> ignore;
});

describe("xtzToTokenTokenOutput", () => {
  test("", () => {
    let result =
      xtzToTokenTokenOutput(
        Mutez(1005000L),
        Mutez(27267104210L),
        Tezos.Token.mkToken(Bigint.of_string("61835018089228411319"), 18),
      )
      |> Utils.map(Tezos.Token.toString);
    expect(result) |> toEqual(Some("0.002272169550648749"));
  })
});

describe("xtzToTokenExchangeRate", () => {
  test("", () => {
    let xtzIn = Tezos.Mutez.Mutez(100000L);
    let xtzPool = Tezos.Mutez.Mutez(2000000L);
    let tokenPool = Tezos.Token.mkToken(Bigint.of_int64(100000L), 0);

    let result = xtzToTokenExchangeRate(xtzIn, xtzPool, tokenPool);

    expect(result) |> toEqual(0.04748);
  });

  test("", () => {
    let xtzIn = Tezos.Mutez.Mutez(10000000L);
    let xtzPool = Tezos.Mutez.Mutez(30000000000L);
    let tokenPool = Tezos.Token.mkToken(Bigint.of_int64(2000032L), 0);

    let result = xtzToTokenExchangeRate(xtzIn, xtzPool, tokenPool);

    expect(result) |> toEqual(0.0000664);
  });

  test("", () => {
    let xtzIn = Tezos.Mutez.Mutez(10000000L);
    let xtzPool = Tezos.Mutez.Mutez(30000000000L);
    let tokenPool = Tezos.Token.mkToken(Bigint.of_int64(2000032L), 8);

    let result = xtzToTokenExchangeRate(xtzIn, xtzPool, tokenPool);

    expect(result) |> toEqual(0.0000664);
  });
});

describe("xtzToTokenExchangeRateForDisplay", () => {
  test("", () => {
    let xtzIn = Tezos.Mutez.Mutez(100000L);
    let xtzPool = Tezos.Mutez.Mutez(2000000L);
    let tokenPool = Tezos.Token.mkToken(Bigint.of_int64(100000L), 0);

    let result = xtzToTokenExchangeRateForDisplay(xtzIn, xtzPool, tokenPool);

    expect(result) |> toEqual(47480.00000000001);
  });

  test("", () => {
    let xtzIn = Tezos.Mutez.Mutez(100000L);
    let xtzPool = Tezos.Mutez.Mutez(2000000L);
    let tokenPool = Tezos.Token.mkToken(Bigint.of_int64(100000L), 8);

    let result = xtzToTokenExchangeRateForDisplay(xtzIn, xtzPool, tokenPool);

    expect(result) |> toEqual(0.00047480000000000005);
  });

  test("", () => {
    let xtzIn = Tezos.Mutez.Mutez(10000000L);
    let xtzPool = Tezos.Mutez.Mutez(30000000000L);
    let tokenPool = Tezos.Token.mkToken(Bigint.of_int64(2000032L), 0);

    let result = xtzToTokenExchangeRateForDisplay(xtzIn, xtzPool, tokenPool);

    expect(result) |> toEqual(66.4);
  });
});

describe("xtzToTokenMinimumTokenOutput", () => {
  test("1000, 1.0%", () =>
    expect(
      xtzToTokenMinimumTokenOutput(
        Tezos.Token.mkToken(Bigint.of_int(1000), 0),
        1.0,
      ),
    )
    |> toEqual(Tezos.Token.mkToken(Bigint.of_int(990), 0))
  );

  test("5000, 20.0%", () =>
    expect(
      xtzToTokenMinimumTokenOutput(
        Tezos.Token.mkToken(Bigint.of_int(5000), 0),
        20.0,
      ),
    )
    |> toEqual(Tezos.Token.mkToken(Bigint.of_int(4000), 0))
  );

  test("100, 5.5%", () =>
    expect(
      xtzToTokenMinimumTokenOutput(
        Tezos.Token.mkToken(Bigint.of_int(100), 0),
        5.5,
      ),
    )
    |> toEqual(Tezos.Token.mkToken(Bigint.of_int(94), 0))
  );
});

describe("tokenToXtzMinimumXtzOutput", () => {
  test("1000, 1.0%", () =>
    expect(tokenToXtzMinimumXtzOutput(Tezos.Mutez.ofIntUnsafe(1000), 1.0))
    |> toEqual(Tezos.Mutez.ofIntUnsafe(990))
  );

  test("5000, 20.0%", () =>
    expect(tokenToXtzMinimumXtzOutput(Tezos.Mutez.ofIntUnsafe(5000), 20.0))
    |> toEqual(Tezos.Mutez.ofIntUnsafe(4000))
  );

  test("100, 5.5%", () =>
    expect(tokenToXtzMinimumXtzOutput(Tezos.Mutez.ofIntUnsafe(100), 5.5))
    |> toEqual(Tezos.Mutez.ofIntUnsafe(94))
  );

  test("16.00000 XTZ, 1.24%", () =>
    expect(
      tokenToXtzMinimumXtzOutput(Tezos.Mutez.ofIntUnsafe(1600000), 1.24),
    )
    |> toEqual(Tezos.Mutez.ofIntUnsafe(1580160))
  );

  test("0.498742 XTZ, 0.31%", () =>
    expect(
      tokenToXtzMinimumXtzOutput(Tezos.Mutez.ofIntUnsafe(498742), 0.31),
    )
    |> toEqual(Tezos.Mutez.ofIntUnsafe(497195))
  );
});

describe("xtzToTokenXtzInput", () => {
  test("", () =>
    expect(
      xtzToTokenXtzInput(
        Tezos_Token.ofInt64(2000000L, 8) |> Tezos_Util.unwrapResult,
        Tezos.Mutez.ofInt64(38490742927L) |> Tezos_Util.unwrapResult,
        Tezos_Token.ofInt64(44366268L, 8) |> Tezos_Util.unwrapResult,
      ),
    )
    |> toEqual(
         Some(Tezos.Mutez.ofInt64(1822514204L) |> Tezos_Util.unwrapResult),
       )
  )
});

describe("tokenToXtzTokenInput", () => {
  test("tzbtc", () =>
    expect(
      tokenToXtzTokenInput(
        Tezos_Mutez.ofInt64(12000000L) |> Tezos_Util.unwrapResult,
        Tezos_Mutez.ofInt64(38490742927L) |> Tezos_Util.unwrapResult,
        Tezos_Token.ofInt64(44366268L, 8) |> Tezos_Util.unwrapResult,
      ),
    )
    |> toEqual(
         Some(Tezos_Token.ofInt64(13877L, 8) |> Tezos_Util.unwrapResult),
       )
  );

  test("usdtz", () =>
    expect(
      tokenToXtzTokenInput(
        Tezos_Mutez.ofInt64(1000000L) |> Tezos_Util.unwrapResult,
        Tezos_Mutez.ofInt64(3003926688L) |> Tezos_Util.unwrapResult,
        Tezos_Token.ofInt64(667902216L, 6) |> Tezos_Util.unwrapResult,
      ),
    )
    |> toEqual(
         Some(Tezos_Token.ofInt64(223086L, 6) |> Tezos_Util.unwrapResult),
       )
  );
});

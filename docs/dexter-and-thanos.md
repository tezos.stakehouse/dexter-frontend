# Dexter and Thanos

This document explains how to use the Dexter web frontend with Thanos. No tokens of monetary value will be used. The purpose of this document is to show how to install Thanos, and connect Thanos to Dexter. 

You can find a video illustration of these instructions [here](https://www.youtube.com/watch?v=7DoVqJfZDtg). Note the video instructions include a demonstration on funding your Thanos wallet with the alphanet [faucet](https://faucet.tzalpha.net/). When using Thanos with Mainnet you can either import an existing wallet, or create a new wallet and send Mainnet XTZ to your Thanos address. 

## Step 1: Installing Thanos

1. Download and install the latest Thanos version for your browser [here](https://thanoswallet.com/download).

<img src="./images/thanos-install.png">

2. Once installed you will see a Thanos landing page that will give you the option to import an existing wallet or a new wallet. Choose your desired option.


<img src="./images/thanos-landing.png">

## Step 2: Connecting Thanos With Dexter

1. Navigate to [Dexter](https://app.dexter.exchange).

2. Click the `Connect Wallet` button. 

<img src="./images/thanos-connect.png">

3. You will see Thanos pop up and ask you to confirm the connection. Click `Connect`. 

<img src="./images/thanos-confirm.png">

4. Your wallet is now connected with its respective balances showing in Dexter. 

<img src="./images/thanos-dexter.png" width="60%">


## Additional Resources
For more information about Thanos please see the official Thanos [website](https://thanoswallet.com/). 
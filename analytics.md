# Wallet
- [x] Connect Wallet
- [ ] Close Tab
- [ ] Limbo

# Add Liquidity
- [x] Click Add (success, public key, blockchain timestamp, wallet error message)

# Remove Liquidity
- [x] Click Remove (success, public key, blockchain timestamp, wallet error message)

# Swap
- [x] Click Swap (success, public key, blockchain timestamp, wallet error message)

# Transactions
- [x] Swap Transaction Added (from amount, from token type, to amount, to token type, match user intent)
- [x] Add Liquidity Transaction Added (from amount, from token type, to amount, to token type, match user intent)
- [x] Remove Liquidity Transaction Added (from amount, from token type,  match user intent)

# Account
- [ ] Click Reload (success, public key, blockchain timestamp, fa, xtz, pool, exchange balance, wallet error message)
- [ ] Click Explore Block (public key, block, explorer url)

# Settings
- [ ] Click Settings
- [ ] Enter Node Address (node address)

# Tutorials
- [ ] Click Tutorials

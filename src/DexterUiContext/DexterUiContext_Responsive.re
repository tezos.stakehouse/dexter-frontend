type breakpointType =
  | XXS // max 424px
  | XS // max 576px
  | SM // max 767px
  | MD // max 991px
  | LG // max 1199px
  | XL;

let checkBreakpoint = _ =>
  if (Window.windowWidth <= 424) {
    XXS;
  } else if (Window.windowWidth <= 576) {
    XS;
  } else if (Window.windowWidth <= 767) {
    SM;
  } else if (Window.windowWidth <= 991) {
    MD;
  } else if (Window.windowWidth <= 1199) {
    LG;
  } else {
    XL;
  };

let ctx = React.createContext(checkBreakpoint());

module ProviderInternal = {
  let makeProps = (~value, ~children, ()) => {
    "value": value,
    "children": children,
  };

  let make = React.Context.provider(ctx);
};

module Provider = {
  [@react.component]
  let make = (~children: React.element) => {
    let (breakpoint, setBreakpoint) = React.useState(() => checkBreakpoint());

    let setValues = _ => {
      let currentBreakpoint = checkBreakpoint();

      if (currentBreakpoint !== breakpoint) {
        setBreakpoint(_ => currentBreakpoint);
      };
    };

    React.useEffect0(() => {
      setValues();
      Window.addSyntheticEventListener("resize", _ => setValues());
      Some(
        () => Window.removeSyntheticEventListener("resize", _ => setValues()),
      );
    });

    <ProviderInternal value=breakpoint> children </ProviderInternal>;
  };
};

let getValue = (~xxs=None, ~xs=None, ~sm=None, ~md=None, ~lg=None, default) => {
  switch (xxs, xs, sm, md, lg) {
  | (Some(xxs), _, _, _, _) => xxs
  | (_, Some(xs), _, _, _) => xs
  | (_, _, Some(sm), _, _) => sm
  | (_, _, _, Some(md), _) => md
  | (_, _, _, _, Some(lg)) => lg
  | _ => default
  };
};

let useValue = (~xxs=None, ~xs=None, ~sm=None, ~md=None, ~lg=None, default) => {
  let newBreakpoint = React.useContext(ctx);
  let newValue =
    switch (newBreakpoint) {
    | XXS => getValue(~xxs, ~xs, ~sm, ~md, ~lg, default)
    | XS => getValue(~xs, ~sm, ~md, ~lg, default)
    | SM => getValue(~sm, ~md, ~lg, default)
    | MD => getValue(~md, ~lg, default)
    | LG => getValue(~lg, default)
    | XL => getValue(default)
    };

  let ((breakpoint, value), setValue) =
    React.useState(() => (newBreakpoint, newValue));

  React.useEffect1(
    _ => {
      if (newBreakpoint !== breakpoint) {
        setValue(_ => (newBreakpoint, newValue));
      };
      None;
    },
    [|newBreakpoint|],
  );

  value;
};

module Device = {
  type t = {
    isXxs: bool,
    isXs: bool,
    isSm: bool,
    isMd: bool,
  };
};

let useDevice = (): Device.t => {
  isXxs: useValue(~xxs=Some(true), false),
  isXs: useValue(~xs=Some(true), false),
  isSm: useValue(~sm=Some(true), false),
  isMd: useValue(~md=Some(true), false),
};

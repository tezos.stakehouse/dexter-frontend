/**
 * Indexter is a service that retrieves the latest values from FA1.2 view entry
 * points.
 */

/**
 * For every endpoint except /currencies there is a new URL parameter network
 * where you can pass either mainnet or edonet and it will
 * automatically change networks for you. Default is mainnet.
 *
 * /currencies has new URL parameter env which supports prod, staging, and dev.  Default is prod.
 */

/*
 | PATH                                         | Description                                          | type        | error    |
 |----------------------------------------------|------------------------------------------------------|:-----------:|:--------:|
 | /fa12/{contract}/balance/{owner}             | Gets the balance of an {owner} in an FA1.2 {contract}| int (MUTEZ) | string   |
 | /fa12/{contract}/supply                      | Gets the supply of an FA1.2 {contract}               | int (MUTEZ) | string   |
 | /fa12/{contract}/allowance/{owner}/{spender} | Gets the allowance of a {spender} for an {owner} in an FA1.2 {contract}  | int (MUTEZ) | string   |
 */

type route =
  | Balance(Tezos.Contract.t, Tezos.Address.t)
  | BalanceNow(Tezos.Contract.t, Tezos.Address.t)
  | TotalSupply(Tezos.Contract.t)
  | Allowance(Tezos.Contract.t, Tezos.Address.t, Tezos.Address.t);

let toRoute = (route: route) =>
  (
    Dexter_Settings.indexterUrl
    ++ (
      switch (route) {
      | Balance(contract, owner) =>
        "/fa12/"
        ++ Tezos.Contract.toString(contract)
        ++ "/balance/"
        ++ Tezos.Address.toString(owner)
      | BalanceNow(contract, owner) =>
        "/fa12/"
        ++ Tezos.Contract.toString(contract)
        ++ "/balance/"
        ++ Tezos.Address.toString(owner)
        ++ "/now"
      | TotalSupply(contract) =>
        "/fa12/" ++ Tezos.Contract.toString(contract) ++ "/supply"
      | Allowance(contract, owner, spender) =>
        "/fa12/"
        ++ Tezos.Contract.toString(contract)
        ++ "/allowance/"
        ++ Tezos.Address.toString(owner)
        ++ "/"
        ++ Tezos.Address.toString(spender)
      }
    )
  )
  ++ "?network="
  ++ Dexter_Settings.indexterNetwork;

let getBalance =
    (
      ~decimals: int,
      ~contract: Tezos.Contract.t,
      ~owner: Tezos.Address.t,
      ~bypassCache: bool=false,
      (),
    )
    : Js.Promise.t(Belt.Result.t(Tezos_Token.t, string)) =>
  Common.fetch(
    ~decode=Tezos_Token.decodeFromString(decimals),
    ~identifier="Indexter.getBalance",
    ~url=
      toRoute(
        bypassCache ? BalanceNow(contract, owner) : Balance(contract, owner),
      ),
    ~requestInit=
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    ~service=Service.Indexter,
    (),
  );

let getTotalSupply =
    (~decimals: int, ~contract: Tezos.Contract.t, ())
    : Js.Promise.t(Belt.Result.t(Tezos_Token.t, string)) =>
  Common.fetch(
    ~decode=Tezos_Token.decodeFromString(decimals),
    ~identifier="Indexter.getTotalSupply",
    ~url=toRoute(TotalSupply(contract)),
    ~requestInit=
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    ~service=Service.Indexter,
    (),
  );

let getAllowance =
    (
      ~decimals: int,
      ~contract: Tezos.Contract.t,
      ~owner: Tezos.Address.t,
      ~spender: Tezos.Address.t,
      (),
    )
    : Js.Promise.t(Belt.Result.t(Tezos_Token.t, string)) =>
  Common.fetch(
    ~decode=Tezos_Token.decodeFromString(decimals),
    ~identifier="Indexter.getAllowance",
    ~url=toRoute(Allowance(contract, owner, spender)),
    ~requestInit=
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    ~service=Service.Indexter,
    (),
  );

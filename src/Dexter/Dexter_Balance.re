/**
 * unify amout of xtz held by an account and dexter exchange balance in a single type.
 */

type t =
  | XtzBalance(Tezos.Mutez.t)
  | ExchangeBalance(Dexter_ExchangeBalance.t);

let isXtz = (t: t) =>
  switch (t) {
  | XtzBalance(_) => true
  | ExchangeBalance(_) => false
  };

let notXtz = (t: t) => !isXtz(t);

let getXtz = (ts: list(t)): t =>
  Belt.List.head(Belt.List.keep(ts, isXtz))
  |> Utils.orDefault(XtzBalance(Tezos.Mutez.zero));

let getXtz1 = (ts: list(t)): option(Tezos.Mutez.t) =>
  switch (Belt.List.head(Belt.List.keep(ts, isXtz))) {
  | Some(XtzBalance(b)) => Some(b)
  | _ => None
  };

let getTokens = (ts: list(t)): list(t) => Belt.List.keep(ts, notXtz);

let getTokens1 = (ts: list(t)): list(Dexter_ExchangeBalance.t) =>
  ts
  |> List.map(balance =>
       switch (balance) {
       | ExchangeBalance(exchangeBalance) => Some(exchangeBalance)
       | _ => None
       }
     )
  |> Array.of_list
  |> Utils.filterNone
  |> Array.to_list;

let getFirstToken = (ts: list(t)): t =>
  Belt.List.head(ts |> getTokens)
  |> Utils.orDefault(XtzBalance(Tezos.Mutez.zero));

let getFirstToken1 = (ts: list(t)): option(Dexter_ExchangeBalance.t) =>
  switch (Belt.List.head(Belt.List.keep(ts, notXtz))) {
  | Some(ExchangeBalance(b)) => Some(b)
  | _ => None
  };

let isPositive = (t: t): bool =>
  switch (t) {
  | XtzBalance(xtz) => xtz |> Tezos.Mutez.gtZero
  | ExchangeBalance(exchangeBalance) =>
    exchangeBalance.tokenBalance |> Tezos.Token.gtZero
  };

let getPositive = (ts: list(t)): list(t) => Belt.List.keep(ts, isPositive);

let isPositiveLqt = (t: t): bool =>
  switch (t) {
  | XtzBalance(_) => false
  | ExchangeBalance(exchangeBalance) =>
    exchangeBalance.lqtBalance |> Tezos.Token.gtZero
  };

let getPositiveLqt = (ts: list(t)): list(t) =>
  Belt.List.keep(ts, isPositiveLqt);

let getTokenByContract =
    (c: Tezos.Contract.t, ts: list(t)): option(Dexter_ExchangeBalance.t) =>
  switch (
    Belt.List.head(
      Belt.List.keep(ts, t => {
        switch (t) {
        | XtzBalance(_) => true
        | ExchangeBalance(e) => e.tokenContract == c
        }
      }),
    )
  ) {
  | Some(ExchangeBalance(b)) => Some(b)
  | _ => None
  };

let getSymbol = (t: t) =>
  switch (t) {
  | XtzBalance(_) => "XTZ"
  | ExchangeBalance(token) => token.symbol
  };

let getName = (t: t) =>
  switch (t) {
  | XtzBalance(_) => "Tez"
  | ExchangeBalance(token) => token.name
  };

let getIconDark = (t: t) =>
  switch (t) {
  | XtzBalance(_) => Tezos.Mutez.icon
  | ExchangeBalance(token) => token.iconDark
  };

let getIconLight = (t: t) =>
  switch (t) {
  | XtzBalance(_) => Tezos.Mutez.icon
  | ExchangeBalance(token) => token.iconLight
  };

let getAccountBalanceAsString = (t: t) =>
  switch (t) {
  | XtzBalance(mutez) => Tezos.Mutez.toTezStringWithCommas(mutez)
  | ExchangeBalance(balance) =>
    Tezos.Token.toStringWithCommas(balance.tokenBalance)
  };

let getExchangeTotalXtzAsString = (t: t) =>
  switch (t) {
  | XtzBalance(_) => ""
  | ExchangeBalance(balance) =>
    balance.exchangeXtz |> Tezos.Mutez.toTezStringWithCommas
  };

let getAccountBalanceAsStringTruncated = (t: t) =>
  switch (t) {
  | XtzBalance(mutez) => Tezos.Mutez.toTezStringWithCommas(mutez)
  | ExchangeBalance(balance) when balance.symbol === "tzBTC" =>
    balance.tokenBalance |> Tezos.Token.toStringWithCommas
  | ExchangeBalance(balance) =>
    balance.tokenBalance |> Tezos.Token.toStringWithCommasTruncated(2)
  };

let getExchangeTotalTokenAsStringTruncated = (t: t) =>
  switch (t) {
  | XtzBalance(_) => ""
  | ExchangeBalance(balance) when balance.symbol === "tzBTC" =>
    balance.exchangeTokenBalance |> Tezos.Token.toStringWithCommas
  | ExchangeBalance(balance) =>
    balance.exchangeTokenBalance |> Tezos.Token.toStringWithCommasTruncated(2)
  };

let getExchangeTotalTokenAsString = (t: t) =>
  switch (t) {
  | XtzBalance(_) => ""
  | ExchangeBalance(balance) =>
    balance.exchangeTokenBalance |> Tezos.Token.toStringWithCommas
  };

let getLiquidityAsString = (t: t) =>
  switch (t) {
  | XtzBalance(_) => ""
  | ExchangeBalance(balance) =>
    Tezos.Token.toStringWithCommas(balance.lqtBalance)
  };

let getExchangeTotalLiquidityAsString = (t: t) =>
  switch (t) {
  | XtzBalance(_) => ""
  | ExchangeBalance(balance) =>
    Tezos.Token.toStringWithCommas(balance.exchangeTotalLqt)
  };

let equal = (t1: t, t2: t) => {
  switch (t1) {
  | XtzBalance(_) =>
    switch (t2) {
    | XtzBalance(_) => true
    | ExchangeBalance(_) => false
    }
  | ExchangeBalance(t1) =>
    switch (t2) {
    | XtzBalance(_) => false
    | ExchangeBalance(t2) =>
      Tezos_Contract.equal(t1.tokenContract, t2.tokenContract)
    }
  };
};

let findBalance = (x: t, ys: list(t)): option(t) => {
  Belt.List.head(Belt.List.keep(ys, y => equal(y, x)));
};

let findBalanceBySymbol = (symbol: string, ys: list(t)): option(t) => {
  Belt.List.head(Belt.List.keep(ys, y => y |> getSymbol === symbol));
};

/**
 * remove a token balance from a list of tokens
 */
let removeBalance = (b: t, balances: list(t)) => {
  Belt.List.keep(balances, a
    => !equal(a, b)); // || isXtz(b));
};

let removeXtz = (balances: list(t)) => Belt.List.keep(balances, notXtz);

let getDecimals = t => {
  switch (t) {
  | XtzBalance(_) => 6
  | ExchangeBalance(token) => token.decimals
  };
};

let getInitialValue = (t: t): Valid.t(InputType.t) =>
  t |> isXtz
    ? Valid(Mutez(Tezos.Mutez.zero), "0")
    : Valid(Token(Tezos.Token.zero), "0");

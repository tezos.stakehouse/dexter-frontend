let getMutezValue = (mutez: Tezos.Mutez.t): Valid.t(Tezos.Mutez.t) =>
  Valid(mutez, Tezos.Mutez.toTezString(mutez));

let getTokenValue = (token: Tezos.Token.t): Valid.t(Tezos.Token.t) =>
  Valid(token, Tezos.Token.toString(token));

let getMutezInputValue = (mutez: Tezos.Mutez.t): Valid.t(InputType.t) =>
  Valid(Mutez(mutez), Tezos.Mutez.toTezString(mutez));

let getTokenInputValue = (token: Tezos.Token.t): Valid.t(InputType.t) =>
  Valid(Token(token), Tezos.Token.toString(token));

let mutezZeroValue = getMutezValue(Tezos.Mutez.zero);
let tokenZeroValue = getTokenValue(Tezos.Token.zero);
let mutezZeroInputValue = getMutezInputValue(Tezos.Mutez.zero);
let tokenZeroInputValue = getTokenInputValue(Tezos.Token.zero);

let getZeroInputValue = (balance: Dexter.Balance.t): Valid.t(InputType.t) =>
  switch (balance) {
  | XtzBalance(_) => mutezZeroInputValue
  | ExchangeBalance(_) => tokenZeroInputValue
  };

let inputValueToStringWithCommas = (input: Valid.t(InputType.t)) => {
  switch (input) {
  | Valid(Mutez(mutez), _) => Tezos.Mutez.toTezStringWithCommas(mutez)
  | Valid(Token(token), _) => Tezos.Token.toStringWithCommas(token)
  | _ => ""
  };
};

let inputValueMinimum = (input: Valid.t(InputType.t), slippage: float) =>
  switch (input) {
  | Valid(Mutez(mutez), _) =>
    Dexter_Exchange.tokenToXtzMinimumXtzOutput(mutez, slippage)
    |> Tezos.Mutez.toTezStringWithCommas
  | Valid(Token(token), _) =>
    Dexter_Exchange.xtzToTokenMinimumTokenOutput(token, slippage)
    |> Tezos.Token.toStringWithCommas
  | _ => ""
  };

let inputValueFee = (input: Valid.t(InputType.t)) => {
  switch (input) {
  | Valid(Mutez(mutez), _) =>
    Dexter_Exchange.xtzFee(mutez) |> Tezos.Mutez.toTezStringWithCommas
  | Valid(Token(token), _) =>
    Dexter_Exchange.tokenFee(token) |> Tezos.Token.toStringWithCommas
  | _ => ""
  };
};

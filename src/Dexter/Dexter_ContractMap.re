type x = {
  exchangeContractName: string,
  exchangeContract: Tezos.Contract.t,
  tokenContractName: string,
  tokenContract: Tezos.Contract.t,
};

type nonEmpty('a) = {
  first: 'a,
  rest: list('a),
};

type t = Belt.Map.String.t((Tezos.Contract.t, Tezos.Contract.t));

let ofList =
    (
      cs:
        list(
          (
            string,
            (
              Belt.Result.t(Tezos.Contract.t, string),
              Belt.Result.t(Tezos.Contract.t, string),
            ),
          ),
        ),
    )
    : option(t) => {
  let (errors: list(string), xs) =
    List.fold_left(
      (
        (
          errors: list(string),
          xs: list((string, (Tezos.Contract.t, Tezos.Contract.t))),
        ),
        c: (
          string,
          (
            Belt.Result.t(Tezos.Contract.t, string),
            Belt.Result.t(Tezos.Contract.t, string),
          ),
        ),
      ) => {
        let (title, (rExchange, rToken)) = c;
        switch (rExchange, rToken) {
        | (Belt.Result.Ok(exchange), Belt.Result.Ok(token)) => (
            errors,
            List.concat([xs, [(title, (exchange, token))]]),
          )
        | _ =>
          let exchangeError =
            switch (rExchange) {
            | Belt.Result.Error(error) => [error]
            | Belt.Result.Ok(_value) => []
            };
          let tokenError =
            switch (rToken) {
            | Belt.Result.Error(error) => [error]
            | Belt.Result.Ok(_) => []
            };
          (List.concat([errors, exchangeError, tokenError]), xs);
        };
      },
      (
        []: list(string),
        []: list((string, (Tezos.Contract.t, Tezos.Contract.t))),
      ),
      cs,
    );

  if (List.length(errors) > 0) {
    Js.Console.log(errors);
    None;
  } else {
    Some(Belt.Map.String.fromArray(xs |> Array.of_list));
  };
};

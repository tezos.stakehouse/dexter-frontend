let exchanges: list(Dexter_Exchange.t) =
  switch (Common.Deployment.get()) {
  | Production
  | Staging => [
      {
        name: "tzBTC",
        symbol: "tzBTC",
        decimals: 8,
        iconDark: "tzBTC-logo.png",
        iconLight: "tzBTC-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1BGQR7t4izzKZ7eRodKWTodAsM23P38v7N")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(541),
      },
      {
        name: "ETH Tez",
        symbol: "ETHtz",
        decimals: 18,
        iconDark: "ethtz-dark.png",
        iconLight: "ethtz-light.png",
        tokenContract:
          Tezos.Contract.ofString("KT19at7rQUvyjxnZ2fBv7D9zc8rkyG7gAoU8")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1PDrBE59Zmxnb8vXRgRAG1XmvTMTs5EDHU")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(542),
      },
      {
        name: "USD Tez",
        symbol: "USDtz",
        decimals: 6,
        iconDark: "usdtz-dark.png",
        iconLight: "usdtz-light.png",
        tokenContract:
          Tezos.Contract.ofString("KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1Tr2eG3eVmPRbymrbU2UppUmKjFPXomGG9")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(543),
      },
      {
        name: "Wrapped Tezos",
        symbol: "wXTZ",
        decimals: 6,
        iconDark: "wXTZ-logo.png",
        iconLight: "wXTZ-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1VYsVfmobT7rsMVivvZ4J8i3bPiqz12NaH")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1D56HQfMmwdopmFLTwNHFJSs6Dsg2didFo")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(544),
      },
      {
        name: "Kolibri",
        symbol: "KUSD",
        decimals: 18,
        iconDark: "kolibri.png",
        iconLight: "kolibri.png",
        tokenContract:
          Tezos.Contract.ofString("KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1AbYeDbjjcAnV1QK7EZUUdqku77CdkTuv6")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(540),
      },
    ]
  | PublicDevelopment
  | PrivateDevelopment
  | NextProtocol => [
      {
        name: "tzBTC",
        symbol: "tzBTC",
        decimals: 8,
        iconDark: "tzBTC-logo.png",
        iconLight: "tzBTC-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1CUg39jQF8mV6nTMqZxjUUZFuz1KXowv3K")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1BYYLfMjufYwqFtTSYJND7bzKNyK7mjrjM")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(14986),
      },
      {
        name: "USD Tez",
        symbol: "USDtz",
        decimals: 6,
        iconDark: "USDtz-logo.png",
        iconLight: "USDtz-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1FCMQk44tEP9fm9n5JJEhkSk1TW3XQdaWH")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1RfTPvrfAQDGAJ7wB71EtwxLQgjmfz59kE")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(14963),
      },
    ]
  };

ErrorReporting.Sentry.init();

/** dependencies **/

let beaconNetwork: Beacon.Network.t =
  switch (Common.Deployment.get()) {
  | Production
  | Staging => {type_: Beacon.Network.Type.Mainnet, name: None, rpcUrl: None}
  | PublicDevelopment
  | PrivateDevelopment
  | NextProtocol => {
      type_: Beacon.Network.Type.Custom,
      name: Some("edonet giganode"),
      rpcUrl: Some("https://edonet-tezos.giganode.io"),
    }
  };

let indexterUrl =
  switch (Common.Deployment.get()) {
  | Production
  | Staging => "https://indexter.dexter.exchange"
  | PublicDevelopment
  | PrivateDevelopment
  | NextProtocol => "https://dev.indexter.dexter.exchange"
  };

let indexterNetwork =
  switch (Common.Deployment.get()) {
  | Production
  | Staging => "mainnet"
  | PublicDevelopment
  | PrivateDevelopment => "edonet"
  | NextProtocol => "edonet"
  };

let nodeUrl =
  switch (Common.Deployment.get()) {
  | Production
  | Staging => "https://mainnet.camlcase.io"
  | PublicDevelopment
  | PrivateDevelopment => "https://edonet-tezos.giganode.io"
  | NextProtocol => "https://edonet-tezos.giganode.io"
  };

let blockExplorerUrl =
  switch (Common.Deployment.get()) {
  | Production
  | Staging => "https://tezblock.io/block/"
  | PublicDevelopment
  | PrivateDevelopment => "https://edonet.tezblock.io/block/"
  | NextProtocol => "https://edonet.tezblock.io/block/"
  };

let transactionExplorerUrl =
  switch (Common.Deployment.get()) {
  | Production
  | Staging => "https://tezblock.io/transaction/"
  | PublicDevelopment
  | PrivateDevelopment => "https://edonet.tezblock.io/transaction/"
  | NextProtocol => "https://edonet.tezblock.io/transaction/"
  };

let tzktUrl =
  switch (Common.Deployment.get()) {
  | Production
  | Staging => "https://tzkt.io/"
  | PublicDevelopment
  | PrivateDevelopment
  | NextProtocol => "https://edo2net.tzkt.io/"
  };

let tzktApiUrl =
  switch (Common.Deployment.get()) {
  | Production
  | Staging => "https://api.tzkt.io/v1/accounts/"
  | PublicDevelopment
  | PrivateDevelopment
  | NextProtocol => "https://api.edo2net.tzkt.io/v1/accounts/"
  };

type baker = {
  address: string,
  name: string,
};

let knownBakers: list(baker) = [
  {address: "tz1WCd2jm4uSt4vntk4vSuUWoZQGhLcDuR9q", name: "Happy Tezos"},
  {address: "tz1g8vkmcde6sWKaG2NN9WKzCkDM6Rziq194", name: "StakeNow"},
  {
    address: "tz1abmz7jiCV2GH2u81LRrGgAFFgvQgiDiaf",
    name: "Tesselated Geometry",
  },
];

let enableExchangeAndSend = false;
let enableTokenToTokenExchange = false;

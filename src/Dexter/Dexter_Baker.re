type t = {
  name: string,
  address: string,
  iconPath: option(string),
};

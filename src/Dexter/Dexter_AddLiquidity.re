/**
 * Calculations for the main Dexter entry points: exchange, add liquidity and
 * remove liquidity.
 */

/**
 * This should only be called on non-zero numbers.
 */
let toToken = (m: Tezos.Mutez.t) => {
  switch (m) {
  | Mutez(t) => Tezos.Token.mkToken(Bigint.of_int64(t), 0)
  };
};

/**
 * This should only be called on non-zero numbers.
 */
let toMutez = (m: Tezos.Token.t) => {
  Tezos.Mutez.Mutez(Bigint.to_int64(m.value));
};

/**
 * AddLiquidity: Calculate the exact amount of token to deposit for a given
 * XTZ input.
 **/
let addLiquidityTokenInJs: (string, string, string) => Js.Nullable.t(string) = [%bs.raw
  {|
   function (xtzIn, xtzPool, tokenPool) {
     const dc = require('dexter-calculations');
     const b  = dc.addLiquidityTokenIn(xtzIn, xtzPool, tokenPool);
     if (b === null) {
       return null;
     };
     return b.toString();
   }
  |}
];

let addLiquidityTokenIn =
    (xtzIn: Tezos.Mutez.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(Tezos.Token.t) =>
  switch (
    Js.Nullable.toOption(
      addLiquidityTokenInJs(
        Tezos_Mutez.toString(xtzIn),
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
      ),
    )
  ) {
  | Some(str) =>
    switch (Tezos_Token.ofString(str, 0)) {
    | Belt.Result.Ok(t) => Some({...t, decimals: tokenPool.decimals})
    | Belt.Result.Error(_) => None
    }
  | None => None
  };

/**
 * AddLiquidity: Calculate the exact amount of token to deposit for a given
 * token input.
 **/
let addLiquidityXtzInJs: (string, string, string) => Js.Nullable.t(string) = [%bs.raw
  {|
   function (tokenIn, xtzPool, tokenPool) {
     const dc = require('dexter-calculations');
     const b  = dc.addLiquidityXtzIn(tokenIn, xtzPool, tokenPool);
     if (b === null) {
       return null;
     };
     return b.toString();
   }
  |}
];

let addLiquidityXtzIn =
    (tokenIn: Tezos.Token.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(Tezos.Mutez.t) =>
  switch (
    Js.Nullable.toOption(
      addLiquidityXtzInJs(
        Tezos_Token.toStringPlain(tokenIn),
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
      ),
    )
  ) {
  | Some(str) =>
    switch (Tezos_Mutez.ofString(str)) {
    | Belt.Result.Ok(t) => Some(t)
    | Belt.Result.Error(_) => None
    }
  | None => None
  };

/**
 * liquidity is minted based on the amount of xtz included in the add liquidity
 * entry point.
 */
let lqtMinted =
    (
      xtzIn: Tezos.Mutez.t,
      xtzPool: Tezos.Mutez.t,
      totalLiquidity: Tezos.Token.t,
    )
    : Tezos.Token.t =>
  Tezos.Mutez.gtZero(xtzPool)
    ? Tezos.Token.(
        div(mul(toToken(xtzIn), totalLiquidity), toToken(xtzPool))
      )
    : Tezos.Token.zero;

let removeLiquidityXtzOutJs: (string, string, string) => Js.Nullable.t(string) = [%bs.raw
  {|
   function (liquidityBurned, totalLiquidity, xtzPool) {
     const dc = require('dexter-calculations');
     const b  = dc.removeLiquidityXtzOut(liquidityBurned, totalLiquidity, xtzPool);
     if (b === null) {
       return null;
     };
     return b.toString();
   }
  |}
];

let removeLiquidityTokenOutJs:
  (string, string, string) => Js.Nullable.t(string) = [%bs.raw
  {|
   function (liquidityBurned, totalLiquidity, tokenPool) {
     const dc = require('dexter-calculations');
     const b  = dc.removeLiquidityTokenOut(liquidityBurned, totalLiquidity, tokenPool);
     if (b === null) {
       return null;
     };
     return b.toString();
   }
  |}
];

let removeLiquidityXtzTokenOut =
    (
      ~liquidityBurned: Tezos.Token.t,
      ~totalLiquidity: Tezos.Token.t,
      ~tokenPool: Tezos.Token.t,
      ~xtzPool: Tezos.Mutez.t,
      (),
    )
    : (Tezos.Mutez.t, Tezos.Token.t) => {
  let xtz =
    switch (
      Js.Nullable.toOption(
        removeLiquidityXtzOutJs(
          Tezos_Token.toStringPlain(liquidityBurned),
          Tezos_Token.toStringPlain(totalLiquidity),
          Tezos_Mutez.toString(xtzPool),
        ),
      )
    ) {
    | Some(str) =>
      switch (Tezos_Mutez.ofString(str)) {
      | Belt.Result.Ok(t) => t
      | Belt.Result.Error(_) => Tezos.Mutez.zero
      }
    | None => Tezos.Mutez.zero
    };

  let token =
    switch (
      Js.Nullable.toOption(
        removeLiquidityTokenOutJs(
          Tezos_Token.toStringPlain(liquidityBurned),
          Tezos_Token.toStringPlain(totalLiquidity),
          Tezos_Token.toStringPlain(tokenPool),
        ),
      )
    ) {
    | Some(str) =>
      switch (Tezos_Token.ofString(str, 0)) {
      | Belt.Result.Ok(t) => {...t, decimals: tokenPool.decimals}
      | Belt.Result.Error(_) =>
        Tezos.Token.mkToken(Bigint.zero, tokenPool.decimals)
      }
    | None => Tezos.Token.mkToken(Bigint.zero, tokenPool.decimals)
    };
  (xtz, token);
};

let liquidityPercentage =
    (liquidityPool: Tezos.Token.t, totalLiquidity: Tezos.Token.t): float =>
  Tezos.Token.geqZero(totalLiquidity)
    ? {
      Tezos.Token.toFloatWithDecimal(liquidityPool)
      /. Tezos.Token.toFloatWithDecimal(totalLiquidity)
      *. 100.0;
    }
    : 0.0;

let liquidityWhenZeroLqt =
    (xtzValue: Valid.t(Tezos.Mutez.t), xtzPool: Tezos.Mutez.t): Tezos.Token.t => {
  switch (xtzValue) {
  | Valid(xtz, _) => Tezos.Mutez.(toToken(add(xtz, xtzPool)))
  | _ => Tezos.Token.zero
  };
};

let setItemRaw: (string, string) => unit = [%bs.raw
  {|
    function (key, value) {
        return localStorage.setItem(key, value);
    }
    |}
];

let setStringItem = (key: string, value: string): unit =>
  setItemRaw(key, value);

let setItem = (key: string, value: Js.Json.t): unit =>
  setItemRaw(key, Js.Json.stringify(value));

let getItemRaw: string => Js.Nullable.t(string) = [%bs.raw
  {|
    function (key) {
        return localStorage.getItem(key);
    }
    |}
];

let getStringItem = (key: string) => Js.Nullable.toOption(getItemRaw(key));

let getItem = (key: string) =>
  switch (Js.Nullable.toOption(getItemRaw(key))) {
  | Some(valueString) =>
    switch (Js.Json.parseExn(valueString)) {
    | value => Some(value)
    | exception _ => None
    }
  | None => None
  };

let removeItem: string => unit = [%bs.raw
  {|
    function (key) {
        return localStorage.removeItem(key);
    }
    |}
];

module Transaction = {
  let key = "transactionHistory";

  let set = (ts: list(Dexter_Transaction.t)) => {
    setItemRaw(
      key,
      Js.Json.stringify(Json.Encode.list(Dexter_Transaction.encode, ts)),
    );
  };

  let get = (): list(Dexter_Transaction.t) => {
    switch (Js.Nullable.toOption(getItemRaw(key))) {
    | Some(transactionsString) =>
      switch (Js.Json.parseExn(transactionsString)) {
      | json =>
        switch (
          Json.Decode.list(
            a => Tezos_Util.unwrapResult(Dexter_Transaction.decode(a)),
            json,
          )
        ) {
        | transactions => transactions
        | exception (Json.Decode.DecodeError(error)) =>
          Js.log(error);
          [];
        }
      | exception _ => []
      }
    | None => []
    };
  };

  let push = (transaction: Dexter_Transaction.t) => {
    setItemRaw(
      key,
      Js.Json.stringify(
        Json.Encode.list(
          Dexter_Transaction.encode,
          [
            transaction,
            ...get()
               |> List.filter((tr: Dexter_Transaction.t) =>
                    !(
                      tr.walletAddress
                      |> Tezos.Address.isEqual(transaction.walletAddress)
                    )
                  ),
          ],
        ),
      ),
    );
  };

  let getByWalletAddress = (walletAddress: Tezos_Address.t) =>
    get()
    |> List.find_opt((tr: Dexter_Transaction.t) =>
         tr.walletAddress |> Tezos_Address.isEqual(walletAddress)
       );

  let removeByHash = (hash: string) =>
    set(
      get() |> List.filter((tr: Dexter_Transaction.t) => tr.hash !== hash),
    );
};

module WalletType = {
  let key = "wallet_type";
  let set = (wallet: Tezos_Wallet.t) =>
    setStringItem(
      key,
      switch (wallet) {
      | Beacon(_) => "Beacon"
      },
    );

  let get = () => getStringItem(key);

  let remove = () => removeItem(key);
};

module Currency = {
  let key = "currency";
  let set = (currency: string) => setStringItem(key, currency);
  let get = () => getStringItem(key) |> Utils.orDefault(Currency.default);
};

module SelectedTokens = {
  let key = "selected_tokens";
  let set = (tokensHash: string) => setStringItem(key, tokensHash);
  let get = () => getStringItem(key);
};

module DarkMode = {
  let key = "dark_mode";
  let set = (darkMode: bool) =>
    setStringItem(key, darkMode ? "true" : "false");
  let get = () => getStringItem(key) === Some("true") ? true : false;
};

module TransactionTimeout = {
  let key = "transaction_timeout";
  let set = (transactionTimeout: int) =>
    setItemRaw(key, transactionTimeout |> Json.Encode.int |> Json.stringify);
  let get = () =>
    getStringItem(key)
    |> Utils.flatMap(transactionTimeout =>
         transactionTimeout |> int_of_string_opt
       )
    |> Utils.orDefault(20);
};

module MaximumSlippage = {
  /* must be between 0.01 and 100.0 */
  type t = float;

  let encode = (t: t): Js.Json.t => Json.Encode.float(t);

  let decode = (json: Js.Json.t): Belt.Result.t(t, string) =>
    switch (Json.Decode.float(json)) {
    | v => Belt.Result.Ok(v)
    | exception _ =>
      Belt.Result.Error(
        "MaximumSlippage.decode: expected a float " ++ Js.Json.stringify(json),
      )
    };

  let key = "maximum_slippage";

  let get = (): float =>
    switch (Js.Nullable.toOption(getItemRaw(key))) {
    | Some(v) =>
      switch (Js.Json.parseExn(v)) {
      | v =>
        switch (decode(v)) {
        | Belt.Result.Ok(slippage) => slippage
        | _ => Dexter_Slippage.defaultSlippage
        }
      | exception (Json.Decode.DecodeError(error)) =>
        Js.log("MaximumSlippage.get: did not find expected value: " ++ error);
        Dexter_Slippage.defaultSlippage;
      }
    | None =>
      Js.log("MaximumSlippage.get: no key 'maximum_slippage' found.");
      Dexter_Slippage.defaultSlippage;
    };

  let getSlippage = (): Dexter_Slippage.t => get() |> Dexter_Slippage.ofFloat;

  let set = (t: t): unit => setItemRaw(key, Js.Json.stringify(encode(t)));

  let setFromSlippage = (slippage: Dexter_Slippage.t): unit =>
    switch (slippage |> Dexter_Slippage.toFloat) {
    | slippage when slippage > 0. => set(slippage)
    | _ => ()
    };
};

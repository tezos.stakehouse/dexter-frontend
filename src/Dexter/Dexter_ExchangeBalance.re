/**
 * information about a particular dexter exchange, how much xtz, token and lqt it has,
 * and how much token and lqt a particular contract has.
 */

type t = {
  decimals: int,
  dexterBaker: option(Tezos.Address.t),
  dexterContract: Tezos.Contract.t,
  exchangeAllowanceForAccount: Tezos.Token.t,
  exchangeTokenBalance: Tezos.Token.t,
  exchangeTotalLqt: Tezos.Token.t,
  exchangeXtz: Tezos.Mutez.t,
  iconDark: string,
  iconLight: string,
  lqtBalance: Tezos.Token.t,
  name: string,
  symbol: string,
  tokenBalance: Tezos.Token.t,
  tokenContract: Tezos.Contract.t,
};

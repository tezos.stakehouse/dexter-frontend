type t = {
  decimals: int,
  name: string,
  prefixed: bool,
  rate: float,
  sign: option(string),
  symbol: string,
};

let default = "USD";

let popular: array(string) = [|
  "USD",
  "EUR",
  "GBP",
  "JPY",
  "RUB",
  "BTC",
  "ETH",
|];

let toStringWithCommas = (currency: t, tezFloat: float): string => {
  let symbol = currency.sign |> Utils.orDefault(" " ++ currency.symbol);

  let floatValue = tezFloat *. currency.rate;
  let value =
    Js.Float.toFixedWithPrecision(floatValue, ~digits=currency.decimals);

  let value =
    currency.decimals > 2 ? value |> Common.removeRedundantZeros : value;

  let aroundZero = floatValue > 0. && value |> Js.Float.fromString === 0.;

  (aroundZero ? "<" : "")
  ++ (currency.prefixed ? symbol : "")
  ++ (
    aroundZero
      ? Common.lowestValueWithDecimals(currency.decimals)
      : Common.addCommas(value)
  )
  ++ (!currency.prefixed ? symbol : "");
};

let findBySymbolOrDefault = (symbol: string, currencies: list(t)): option(t) =>
  switch (
    currencies |> List.find_opt((currency: t) => currency.symbol === symbol)
  ) {
  | Some(current) => Some(current)
  | _ =>
    currencies |> List.find_opt((currency: t) => currency.symbol === default)
  };

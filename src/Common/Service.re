exception DecodeError(string);

type t =
  | CoinGecko
  | Indexter
  | TezosNode
  | TZKT;

let toString = (t: t) =>
  switch (t) {
  | CoinGecko => "CoinGecko"
  | Indexter => "Indexter"
  | TezosNode => "TezosNode"
  | TZKT => "TZKT"
  };

let encode = (t: t): Js.Json.t => Json.Encode.string(t |> toString);

let decode = (json: Js.Json.t): t => {
  switch (json |> Json.Decode.string) {
  | "CoinGecko" => CoinGecko
  | "Indexter" => Indexter
  | "TezosNode" => TezosNode
  | "TZKT" => TZKT
  | service => raise @@ DecodeError("Expected Service, got " ++ service)
  };
};

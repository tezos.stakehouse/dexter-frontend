[@bs.scope "window"] [@bs.val] external windowWidth: int = "innerWidth";

[@bs.scope "window"] [@bs.val]
external addSyntheticEventListener:
  (string, ReactEvent.Synthetic.t => unit) => unit =
  "addEventListener";

[@bs.scope "window"] [@bs.val]
external removeSyntheticEventListener:
  (string, ReactEvent.Synthetic.t => unit) => unit =
  "removeEventListener";

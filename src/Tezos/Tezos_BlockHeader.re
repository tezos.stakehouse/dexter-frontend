/*
     { "level": integer ∈ [-2^31-2, 2^31+2],
       "proto": integer ∈ [0, 255],
       "predecessor": $block_hash,
       "timestamp": $timestamp.protocol,
       "validation_pass": integer ∈ [0, 255],
       "operations_hash": $Operation_list_list_hash,
       "fitness": $fitness,
       "context": $Context_hash,
       "protocol_data": /^[a-zA-Z0-9]+$/ }
 */

type t = {
  level: int,
  predecessor: string,
  timestamp: Tezos_Timestamp.t,
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  Json.Decode.(switch (
    field("level", int, json),
    field("predecessor", string, json),
    field(
      "timestamp",
      a => Tezos_Util.unwrapResult(Tezos_Timestamp.decode(a)),
      json,
    ),
  ) {
  | (level, predecessor, timestamp) =>
    Belt.Result.Ok({level, predecessor, timestamp})
  | exception (DecodeError(error)) => Belt.Result.Error(error)
  });
};

/**
 * An external node that Dexter will communicate with
 */

type t = {url: string};

let encode = (t: t): Js.Json.t => {
  Json.Encode.object_([("url", Json.Encode.string(t.url))]);
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) =>
  switch ({url: Json.Decode.field("url", Json.Decode.string, json)}) {
  | v => Belt.Result.Ok(v)
  | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err)
  };

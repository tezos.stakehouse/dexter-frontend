/**
 * prepare the approve operation
 */

let mkApproveOperations =
    (
      ~dexterAllowanceForOwner: Tezos.Token.t,
      ~tokenContract: Tezos.Contract.t,
      ~dexterContract: Tezos.Contract.t,
      ~approveAmount: Tezos.Token.t,
      (),
    )
    : list(Tezos_Operation.t) =>
  if (Tezos.Token.lt(dexterAllowanceForOwner, approveAmount)) {
    let approveTransaction: Tezos_Operation.t = {
      kind: Tezos_Operation.Kind.Transaction,
      destination: tokenContract,
      amount: Tezos.Mutez.zero,
      parameters:
        Some({
          entrypoint: "approve",
          value:
            Token_Query.encodeApprove(
              Tezos.Contract.toString(dexterContract),
              approveAmount,
            ),
        }),
    };

    // when the approval is zero you can set it to whatever is necessary
    if (Tezos.Token.equal(dexterAllowanceForOwner, Tezos.Token.zero)) {
      [approveTransaction];
    } else {
      // when the approval is not zero, set it to zero first, then the target value
      let approveTransactionZero: Tezos_Operation.t = {
        kind: Tezos_Operation.Kind.Transaction,
        destination: tokenContract,
        amount: Tezos.Mutez.zero,
        parameters:
          Some({
            entrypoint: "approve",
            value:
              Token_Query.encodeApprove(
                Tezos.Contract.toString(dexterContract),
                Tezos.Token.zero,
              ),
          }),
      };
      [approveTransactionZero, approveTransaction];
    };
  } else {
    [];
  };

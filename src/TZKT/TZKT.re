/* TZKT is a tezos blockchain indexer.
 *
 */

module Status = {
  type t =
    | Applied
    | Backtracked
    | Ready
    | Failed(FailureReason.t)
    | Pending
    | Skipped
    | Invalidated;

  let encode = (t: t): Js.Json.t => {
    (
      switch (t) {
      | Applied => "applied"
      | Backtracked => "backtracked"
      | Ready => "ready"
      | Failed(_) => "failed"
      | Pending => "pending"
      | Skipped => "skipped"
      | Invalidated => "invalidated"
      }
    )
    |> Json.Encode.string;
  };

  let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
    switch (json |> Json.Decode.string) {
    | "applied" => Belt.Result.Ok(Applied)
    | "backtracked" => Belt.Result.Ok(Backtracked)
    | "failed" => Belt.Result.Ok(Failed(Unknown))
    | "ready" => Belt.Result.Ok(Ready)
    | "pending" => Belt.Result.Ok(Pending)
    | "skipped" => Belt.Result.Ok(Skipped)
    | "invalidated" => Belt.Result.Ok(Invalidated)
    | error =>
      Belt.Result.Error("TZKT.Status.decode unknown string: " ++ error)
    | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
    };
  };

  let ofOperationAlphaOperationResult =
      (result: option(Tezos_Operation_Alpha.Operation.Result.Transaction.t))
      : t =>
    switch (result) {
    | Some(Applied(_)) => Applied
    | Some(Failed(reason)) => Failed(reason)
    | Some(Skipped) => Skipped
    | Some(Backtracked) => Backtracked
    | _ => Ready
    };
};

module Operation = {
  type t = {
    type_: string,
    id: int,
    level: int,
    timestamp: string,
    block: string,
    hash: string,
    counter: int,
    initiator: option(Tezos_Address.t), /* Some when different from sender */
    sender: Tezos_Address.t,
    nonce: option(int),
    gasLimit: int,
    gasUsed: int,
    storageLimit: int,
    storageUsed: int,
    allocationFee: int,
    target: Tezos_Address.t,
    amount: Tezos_Mutez.t,
    parameters: option(Tezos_Operation_Alpha.Parameters.t),
    status: Status.t,
  };

  let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
    switch (
      Json.Decode.(
        field("type", string, json),
        field("id", int, json),
        field("level", int, json),
        field("timestamp", string, json),
        field("block", string, json),
        field("hash", string, json),
        field("counter", int, json),
        optional(
          field("initiator", internal_ =>
            field(
              "address",
              x => Tezos_Util.unwrapResult(Tezos_Address.decode(x)),
              internal_,
            )
          ),
          json,
        ),
        field(
          "sender",
          internal_ =>
            field(
              "address",
              x => Tezos_Util.unwrapResult(Tezos_Address.decode(x)),
              internal_,
            ),
          json,
        ),
        optional(field("nonce", int), json),
        field("gasLimit", int, json),
        field("gasUsed", int, json),
        field("storageLimit", int, json),
        field("storageUsed", int, json),
        field("allocationFee", int, json),
        field(
          "target",
          internal_ =>
            field(
              "address",
              x => Tezos_Util.unwrapResult(Tezos_Address.decode(x)),
              internal_,
            ),
          json,
        ),
        field("amount", int, json),
        optional(
          field("parameters", a =>
            Tezos_Util.unwrapResult(
              Tezos_Operation_Alpha.Parameters.decode(
                Js.Json.parseExn(Json.Decode.string(a)),
              ),
            )
          ),
          json,
        ),
        field(
          "status",
          x => Tezos_Util.unwrapResult(Status.decode(x)),
          json,
        ),
      )
    ) {
    | (
        type_,
        id,
        level,
        timestamp,
        block,
        hash,
        counter,
        initiator,
        sender,
        nonce,
        gasLimit,
        gasUsed,
        storageLimit,
        storageUsed,
        allocationFee,
        target,
        amount,
        parameters,
        status,
      ) =>
      switch (Tezos_Mutez.ofInt(amount)) {
      | Belt.Result.Ok(amount) =>
        Belt.Result.Ok({
          type_,
          id,
          level,
          timestamp,
          block,
          hash,
          counter,
          initiator,
          sender,
          nonce,
          gasLimit,
          gasUsed,
          storageLimit,
          storageUsed,
          allocationFee,
          target,
          amount,
          parameters,
          status,
        })
      | Belt.Result.Error(error) =>
        Belt.Result.Error("TZKT.decode amount is out of bounds: " ++ error)
      }
    | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
    };
  };

  let decodeTZKT = json =>
    switch (Json.Decode.list(decode, json)) {
    | v => Common.sequence(v)
    | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err)
    };

  /* example query
   * https://api.tzkt.io/v1/accounts/tz1gV3LBv1XmVEkdeRTjuUdurvCf8P85eALM/operations?type=transaction
   */
  let fetch =
      (address: Tezos_Address.t)
      : Js.Promise.t(Belt.Result.t(list(t), string)) =>
    Common.fetch(
      ~decode=decodeTZKT,
      ~identifier="TZKT.fetch",
      ~url=
        Dexter_Settings.tzktApiUrl
        ++ Tezos_Address.toString(address)
        ++ "/operations?type=transaction&limit=1000",
      ~requestInit=
        Fetch.RequestInit.make(
          ~method_=Get,
          ~headers=
            Fetch.HeadersInit.makeWithArray([|
              ("Accept", "application/json"),
            |]),
          ~credentials=Omit,
          (),
        ),
      ~service=Service.TZKT,
      (),
    );
};

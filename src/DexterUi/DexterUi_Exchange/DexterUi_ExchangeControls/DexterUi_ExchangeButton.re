open DexterUi_Exchange_Utils;

[@react.component]
let make =
    (
      ~disabled: bool,
      ~resetInputs: unit => unit,
      ~sendTo: bool,
      ~sendToAddress: string,
      ~state: DexterUi_Exchange_Reducer.state,
    ) => {
  let {account} = DexterUiContext.Account.useContext();
  let {pushTransaction, transactionTimeout} =
    DexterUiContext.Transactions.useContext();

  <DexterUi_TransactionButton
    px={`px(20)}
    disabled
    onClick={_ =>
      switch (account) {
      | Some(account) =>
        onExchange(
          account,
          pushTransaction,
          resetInputs,
          sendTo,
          sendToAddress,
          state,
          transactionTimeout,
        )
      | _ => ()
      }
    }>
    {sendTo
       ? "Exchange "
         ++ Dexter.Balance.getSymbol(state.inputToken)
         ++ " and send "
         ++ Dexter.Balance.getSymbol(state.outputToken)
         |> React.string
       : "Exchange "
         ++ Dexter.Balance.getSymbol(state.inputToken)
         ++ " for "
         ++ Dexter.Balance.getSymbol(state.outputToken)
         |> React.string}
  </DexterUi_TransactionButton>;
};

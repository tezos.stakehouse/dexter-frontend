[@react.component]
let make =
    (
      ~descriptionHidden: bool,
      ~slippage: Dexter_Slippage.t,
      ~state: DexterUi_Exchange_Reducer.state,
    ) => {
  let {isSm} = DexterUiContext.Responsive.useDevice();
  let hasPositiveBalances = DexterUi_Hooks.useHasPositiveBalances();

  <Flex
    mt={`px(11)} minHeight={isSm ? `initial : `px(30)} alignItems=`flexEnd>
    <DexterUi_Description>
      {switch (!hasPositiveBalances, !descriptionHidden) {
       | (true, _) =>
         "Add some Tezos tokens to your connected wallet to use the Dexter Exchange."
         |> React.string
       | (_, true) =>
         "You are exchanging "
         ++ Dexter_Value.inputValueToStringWithCommas(state.inputValue)
         ++ " "
         ++ Dexter_Balance.getSymbol(state.inputToken)
         ++ " for a minimum of "
         ++ Dexter_Value.inputValueMinimum(
              state.outputValue,
              slippage |> Dexter_Slippage.toFloat,
            )
         ++ " "
         ++ Dexter_Balance.getSymbol(state.outputToken)
         ++ " to a maximum of "
         ++ Dexter_Value.inputValueToStringWithCommas(state.outputValue)
         ++ " "
         ++ Dexter_Balance.getSymbol(state.outputToken)
         ++ " with a price slippage limit of "
         ++ (slippage |> Dexter_Slippage.toString)
         ++ ". A liquidity fee of 0.3% ("
         ++ Dexter_Value.inputValueFee(state.outputValue)
         ++ " "
         ++ Dexter.Balance.getSymbol(state.outputToken)
         ++ ") has been applied to this exchange."
         |> React.string
       | _ => React.null
       }}
    </DexterUi_Description>
  </Flex>;
};

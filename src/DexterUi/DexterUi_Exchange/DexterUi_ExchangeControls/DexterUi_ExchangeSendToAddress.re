[@react.component]
let make =
    (
      ~invalidAddress: bool=false,
      ~outputToken: Dexter_Balance.t,
      ~sendTo: bool,
      ~setSendTo: bool => unit,
      ~sendToAddress: string,
      ~setSendToAddress: (string => string) => unit,
    ) => {
  let hasPositiveBalances = DexterUi_Hooks.useHasPositiveBalances();
  let isLoggedIn = DexterUi_Hooks.useIsLoggedIn();
  let isDisabled = !isLoggedIn || !hasPositiveBalances;

  <Flex justifyContent=`center mt={`px(13)}>
    <Flex flexDirection=`column>
      <Flex
        onClick={_ => setSendTo(!sendTo)} disabled=isDisabled mb={`px(5)}>
        <Flex alignItems=`center>
          <DexterUi_Radio isSelected=sendTo />
          <Flex ml={`px(4)}>
            <Text>
              {"Send "
               ++ Dexter_Balance.getSymbol(outputToken)
               ++ " to another address"
               |> React.string}
            </Text>
          </Flex>
        </Flex>
      </Flex>
      <DexterUi_AddressInput
        isValid={!invalidAddress}
        isDisabled={isDisabled || !sendTo}
        value=sendToAddress
        setValue={value => setSendToAddress(_ => value)}
      />
    </Flex>
  </Flex>;
};

[@react.component]
let make =
    (
      ~localSlippage: Dexter_Slippage.t,
      ~setLocalSlippage: (Dexter_Slippage.t => Dexter_Slippage.t) => unit,
    ) =>
  <Flex alignItems=`center>
    <DexterUi_ExchangeSlippageOption
      option=Dexter_Slippage.PointOne
      slippage=localSlippage
      setSlippage=setLocalSlippage
    />
    <DexterUi_ExchangeSlippageOption
      option=Dexter_Slippage.Half
      slippage=localSlippage
      setSlippage=setLocalSlippage
    />
    <DexterUi_ExchangeSlippageOption
      option=Dexter_Slippage.One
      slippage=localSlippage
      setSlippage=setLocalSlippage
    />
    <DexterUi_ExchangeSlippageOptionsCustom
      slippage=localSlippage
      setSlippage=setLocalSlippage
    />
  </Flex>;

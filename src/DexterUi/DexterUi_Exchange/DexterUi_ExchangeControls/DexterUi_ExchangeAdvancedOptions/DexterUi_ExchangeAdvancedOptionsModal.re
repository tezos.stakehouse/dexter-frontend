[@react.component]
let make =
    (
      ~slippage: Dexter_Slippage.t,
      ~setSlippage: (Dexter_Slippage.t => Dexter_Slippage.t) => unit,
      ~setShowAdvancedOptions: (bool => bool) => unit,
    ) => {
  let {transactionTimeout, setTransactionTimeout} =
    DexterUiContext.Transactions.useContext();
  let (localSlippage, setLocalSlippage) = React.useState(_ => slippage);
  let (localTransactionTimeout, setLocalTransactionTimeout) =
    React.useState(_ => transactionTimeout);

  <>
    <Flex
      position=`fixed
      background={`rgba((0, 0, 0, `num(0.33)))}
      width={`percent(100.)}
      height={`percent(100.)}
      top=`zero
      left=`zero
      zIndex=4
    />
    <Flex
      position=`absolute
      p={`px(20)}
      background=Colors.offWhite
      borderRadius={`px(16)}
      flexDirection=`column
      minWidth={`px(263)}
      top={`px(-36)}
      zIndex=5>
      <DexterUi_ExchangeAdvancedOptionsTitle
        title={"Limit price slippage" |> React.string}
        tooltipText={
          "Some price slippage may occur on large orders. Limiting price slippage may protect your trade from acute price movement, however your order is more likely to fail due to normal price changes."
          |> React.string
        }
      />
      <Flex height={`px(10)} />
      <DexterUi_ExchangeSlippageOptions localSlippage setLocalSlippage />
      <Flex height={`px(18)} />
      <DexterUi_ExchangeAdvancedOptionsTitle
        title={"Transaction timeout" |> React.string}
        tooltipText={
          "Dexter will revert your transaction if it has not been completed in this timeframe."
          |> React.string
        }
      />
      <Flex height={`px(5)} />
      <DexterUi_ExchangeTransactionTimeout
        localTransactionTimeout
        setLocalTransactionTimeout
      />
      <Flex
        alignItems=`center mt={`px(16)} pr={`px(10)} justifyContent=`flexEnd>
        <DexterUi_Button
          disabled={
            switch (localSlippage) {
            | Custom(Invalid(_))
            | Custom(InvalidInitialState) => true
            | _ => localTransactionTimeout > 1440
            }
          }
          darkModeEnabled=false
          small=true
          onClick={_ => {
            setSlippage(_ => localSlippage);
            setTransactionTimeout(localTransactionTimeout);
            setShowAdvancedOptions(_ => false);
          }}
          width={`px(112)}>
          {"Save changes" |> React.string}
        </DexterUi_Button>
        <Flex width={`px(16)} />
        <Flex onClick={_ => setShowAdvancedOptions(_ => false)}>
          <Text
            darkModeColor=Colors.primaryDark
            lightModeColor=Colors.blue
            textDecoration=`underline>
            {"Cancel" |> React.string}
          </Text>
        </Flex>
      </Flex>
    </Flex>
  </>;
};

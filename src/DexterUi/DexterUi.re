include DexterUi_Style;
module MediaQuery = Common_MediaQuery;

[@react.component]
let make = () => {
  Common.isMaintenanceMode()
    ? <DexterUi_MaintenanceMode />
    : <DexterUiContext> <DexterUi_Layout /> </DexterUiContext>;
};
// <>
//   <MediaQuery.Compat query="(max-device-width: 768px)">
//     <DexterUi_Mobile />
//   </MediaQuery.Compat>
//   <MediaQuery.Compat query="(min-device-width: 769px)">
//     {Common.isMaintenanceMode()
//        ? <DexterUi_MaintenanceMode />
//        : <DexterUiContext>
//            <DexterUi_Content />
//            {switch (Common.Deployment.get()) {
//             | PublicDevelopment
//             | PrivateDevelopment => <DexterUi_Development />
//             | _ => React.null
//             }}
//          </DexterUiContext>}
//   </MediaQuery.Compat>
// </>;

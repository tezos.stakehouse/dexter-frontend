open Utils;

[@react.component]
let make =
    (
      ~children: React.element,
      ~displayOnly: bool=false,
      ~isDisabled: bool=false,
      ~isValid: bool=true,
      ~note: option(React.element)=?,
      ~onClickMax: option(_ => unit)=?,
    ) => {
  let {darkMode} = DexterUiContext.Theme.useContext();

  let readOnlyColor = darkMode ? Colors.tabInactiveDark : Colors.lightGrey2;
  let redBorder = !displayOnly && !isValid;
  let withMax = onClickMax |> Belt.Option.isSome;

  <Flex flexDirection=`column>
    <Flex ml={`px(4)} mb={`px(4)}>
      <Text> {"Amount:" |> React.string} </Text>
    </Flex>
    <Flex
      disabled={!displayOnly && isDisabled}
      className=Css.(
        style([
          border(
            darkMode
              ? px(redBorder ? 2 : 1) : pxFloat(redBorder ? 1.5 : 0.5),
            `solid,
            displayOnly
              ? readOnlyColor : redBorder ? Colors.red : Colors.lightGrey,
          ),
        ])
      )
      background=?{displayOnly ? Some(readOnlyColor) : None}
      borderRadius={`px(8)}
      height={`px(54)}
      pl={`pxFloat(darkMode ? redBorder ? 11. : 12. : redBorder ? 11.5 : 12.)}
      pr={
        withMax
          ? `pxFloat(
              darkMode ? redBorder ? 15. : 16. : redBorder ? 15.5 : 16.,
            )
          : `zero
      }
      justifyContent=`center
      flexDirection=`column>
      <Flex
        alignItems=`center
        overflowX=`auto
        borderRadius={`px(8)}
        ml={`px(-2)}
        pl={`px(2)}>
        <Flex
          pr={withMax ? `px(3) : `zero}
          width=?{
            withMax ? Some(`calc((`sub, `percent(100.), `px(20)))) : None
          }>
          {displayOnly
             ? <Flex disabled=isDisabled>
                 <Text
                   darkModeColor=Colors.white
                   lightModeColor=Colors.black
                   textStyle=TextStyles.h1>
                   children
                 </Text>
               </Flex>
             : children}
        </Flex>
        {onClickMax
         |> renderOpt(onClickMax =>
              <Flex onClick=?{isDisabled ? None : Some(_ => onClickMax())}>
                <Text textStyle=TextStyles.bold>
                  {"MAX" |> React.string}
                </Text>
              </Flex>
            )}
      </Flex>
      {note |> renderOpt(note => <Text textAlign=`left> note </Text>)}
    </Flex>
  </Flex>;
};

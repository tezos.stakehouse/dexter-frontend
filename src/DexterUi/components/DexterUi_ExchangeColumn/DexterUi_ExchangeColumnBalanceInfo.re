[@react.component]
let make = (~balanceInfo: list(string)) => {
  let isLoggedIn = DexterUi_Hooks.useIsLoggedIn();

  balanceInfo
  |> List.mapi((i, line) =>
       <Text
         className=Css.(style([opacity(isLoggedIn || i > 0 ? 1. : 0.33)]))
         key={i |> string_of_int}
         whiteSpace=`nowrap>
         // first one is always user's balance

           {i === 0 |> Utils.renderIf("My balance: " |> React.string)}
           {(isLoggedIn || i > 0 ? line : "N/A") |> React.string}
         </Text>
     )
  |> Array.of_list
  |> React.array;
};

open Utils;

[@react.component]
let make =
    (
      ~balanceInfo: option(list(string))=?,
      ~children: React.element,
      ~minHeight: option(Flex.heightType)=?,
      ~title: option(React.element)=?,
      ~balance: Dexter_Balance.t,
      ~isPoolToken: bool=false,
      ~tokenSearchBalances: option(list(Dexter_Balance.t))=?,
      ~tokenSearchOnChange: option(Dexter_Balance.t => unit)=?,
      ~tokenSearchSide: Side.t=Left,
    ) => {
  let {isSm} = DexterUiContext.Responsive.useDevice();

  let (tokenSearchVisible, setTokenSearchVisible) =
    React.useState(_ => false);

  let hideTokenSearch =
    React.useCallback(_ => setTokenSearchVisible(_ => false));

  let (isChangable, tokenSearchOnChange, onClick) =
    switch (tokenSearchOnChange, tokenSearchBalances) {
    | (Some(onTokenChange), Some(tokenSearchBalances))
        when tokenSearchBalances |> List.length > 1 => (
        true,
        Some(
          token => {
            hideTokenSearch();
            onTokenChange(token);
          },
        ),
        Some(_ => setTokenSearchVisible(_ => true)),
      )
    | _ => (false, None, None)
    };

  <Flex
    flexDirection=`column
    flexGrow=1.
    width={isSm ? `initial : `zero}
    ?minHeight>
    {(!isSm || title |> Belt.Option.isSome)
     |> Utils.renderIf(
          <Flex mb={`px(16)}>
            <Text
              textStyle=TextStyles.h3
              darkModeColor=Colors.white
              lightModeColor=Colors.black>
              {title |> orDefault(Common.nbsp)}
            </Text>
          </Flex>,
        )}
    <Flex flexDirection=`column>
      <Flex
        alignItems=`flexStart
        mb={isSm ? `zero : `px(12)}
        ?onClick
        className=Css.(
          style([hover([selector(".icon", [important(opacity(1.))])])])
        )>
        <Flex>
          {switch (isPoolToken, balance) {
           | (true, ExchangeBalance(token)) =>
             <DexterUi_PoolTokenIcon
               icon={token.iconLight}
               iconDark={token.iconDark}
             />
           | _ =>
             <DexterUi_TokenIcon
               icon={balance |> Dexter_Balance.getIconLight}
               iconDark={balance |> Dexter_Balance.getIconDark}
             />
           }}
        </Flex>
        <Flex ml={`px(12)} flexDirection=`column>
          <Flex
            className=Css.(
              style(
                balanceInfo |> Belt.Option.isNone
                  ? [transform(translateY(px(5)))] : [],
              )
            )
            alignItems=`center>
            <Text
              textStyle=TextStyles.h1
              darkModeColor=Colors.white
              lightModeColor=Colors.black
              whiteSpace=`nowrap>
              {switch (isPoolToken, balance) {
               | (true, ExchangeBalance(token)) =>
                 <DexterUi_PoolTokenSymbol token />
               | _ => balance |> Dexter_Balance.getSymbol |> React.string
               }}
            </Text>
            {isChangable
             |> renderIf(
                  <Flex ml={`px(8)} inlineFlex=true>
                    <Text
                      className=Css.(
                        style([
                          transform(scaleY(tokenSearchVisible ? (-1.) : 1.)),
                          opacity(tokenSearchVisible ? 1. : 0.5),
                        ])
                        ++ " icon"
                      )
                      fontSize={`px(10)}>
                      <i className="icon-down-open" />
                    </Text>
                  </Flex>,
                )}
          </Flex>
          <Flex
            flexDirection=`column alignItems=`flexStart minHeight={`px(30)}>
            {balanceInfo
             |> Utils.renderOpt(balanceInfo =>
                  <DexterUi_ExchangeColumnBalanceInfo balanceInfo />
                )}
          </Flex>
        </Flex>
      </Flex>
      {switch (tokenSearchVisible, tokenSearchBalances, tokenSearchOnChange) {
       | (true, Some(balances), Some(onTokenChange)) =>
         <DexterUi_TokenSearch
           balances={balances |> Dexter_Balance.removeBalance(balance)}
           hideTokenSearch
           onTokenChange
           side=tokenSearchSide
         />
       | _ => React.null
       }}
      children
    </Flex>
  </Flex>;
};

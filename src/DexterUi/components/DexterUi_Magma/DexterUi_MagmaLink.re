[@react.component]
let make = () => {
  let {isMd} = DexterUiContext.Responsive.useDevice();

  <Flex
    alignItems=`center
    justifyContent=`center
    flexGrow={isMd ? 0. : 1.}
    mt={`px(12)}
    px={`px(isMd ? 12 : 16)}
    flexWrap=`wrap>
    <Text
      lightModeColor=Colors.darkGrey2
      textStyle=TextStyles.bold
      inlineBlock=true
      spaceAfter=true>
      {"Interested in using Dexter on mobile?" |> React.string}
    </Text>
    <Flex flexWrap=`wrap justifyContent=`center>
      <Text inlineBlock=true spaceAfter=true> {"Try" |> React.string} </Text>
      <DexterUi_Link href="https://magmawallet.io/" ellipsis=false>
        {"Magma" |> React.string}
      </DexterUi_Link>
      <Text inlineBlock=true>
        {", the Tezos wallet app from camlCase," |> React.string}
      </Text>
      <Text inlineBlock=true>
        {"the same team behind Dexter." |> React.string}
      </Text>
    </Flex>
  </Flex>;
};

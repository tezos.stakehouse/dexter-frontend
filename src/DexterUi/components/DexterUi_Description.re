[@react.component]
let make = (~children: React.element) => {
  let {route} = DexterUiContext.Route.useContext();
  let isLoggedIn = DexterUi_Hooks.useIsLoggedIn();
  let isTransactionPending = DexterUi_Hooks.useIsTransactionPending();
  let transactionsDisabled = DexterUiContext.Services.areTransactionsDisable();

  <Text lightModeColor=Colors.grey1 textStyle=TextStyles.h2 textAlign=`center>
    {switch (transactionsDisabled, !isLoggedIn, isTransactionPending) {
     | (true, _, _) =>
       <>
         <Text
           className=Css.(style([verticalAlign(`textBottom)]))
           fontSize={`px(16)}
           lightModeColor=Colors.grey1
           spaceAfter=true>
           {Common.alert |> React.string}
         </Text>
         {"An important connected service is down. We hope to restore access shortly."
          |> React.string}
       </>
     | (_, true, _) =>
       "Please connect a wallet to use the Dexter"
       ++ (
         switch (route) {
         | Exchange => " Exchange."
         | LiquidityPool(_) => " Liquidity Pool."
         | _ => "."
         }
       )
       |> React.string
     | (_, _, true) =>
       "Please wait until the previous transaction is confirmed on the Tezos blockchain."
       |> React.string
     | _ => children
     }}
  </Text>;
};

[@react.component]
let make = () => {
  let {isMd} = DexterUiContext.Responsive.useDevice();
  let {connectToBeacon}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();
  let clicksCounter: ref(int) = ref(0);
  let clicksTimeout: ref(option(Js.Global.timeoutId)) = ref(None);

  let onClick =
    React.useCallback0(_ => {
      clicksTimeout^
      |> Utils.map(timeout => Js.Global.clearTimeout(timeout))
      |> Utils.orDefault();

      clicksTimeout :=
        Some(Js.Global.setTimeout(() => clicksCounter := 0, 500));

      if (clicksCounter^ === 4) {
        connectToBeacon();
      };

      clicksCounter := clicksCounter^ + 1;
    });

  <Flex
    justifyContent={isMd ? `flexEnd : `flexStart}
    pt={`px(12)}
    px={`px(isMd ? 12 : 16)}
    onClick=?{Common.isMobile() ? Some(onClick) : None}>
    <Text> {React.string("v" ++ Common.packageVersion())} </Text>
  </Flex>;
};

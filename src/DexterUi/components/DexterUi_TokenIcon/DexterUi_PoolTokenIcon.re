[@react.component]
let make = (~icon: string, ~iconDark: option(string)=?, ~small: bool=false) =>
  <Flex>
    <Flex> <DexterUi_TokenIcon icon=Tezos.Mutez.icon small /> </Flex>
    <Flex ml={`px(small ? (-8) : (-12))} zIndex=2>
      <DexterUi_TokenIcon icon ?iconDark small />
    </Flex>
  </Flex>;

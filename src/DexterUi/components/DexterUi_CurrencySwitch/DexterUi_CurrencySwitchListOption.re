[@react.component]
let make = (~currency: Currency.t) => {
  let {currentCurrency, setCurrentCurrency} =
    DexterUiContext.Currencies.useContext();

  let onClick = React.useCallback0(_ => setCurrentCurrency(currency));

  <Flex
    alignItems=`center
    background=Colors.white
    borderRadius={`px(4)}
    flexShrink=0.
    full=true
    height={`px(54)}
    justifyContent=`spaceBetween
    mb={`px(8)}
    px={`px(18)}
    onClick>
    <Flex alignItems=`center>
      <Text textStyle=TextStyles.h3 color=Colors.black>
        {currency.symbol |> React.string}
      </Text>
      <Flex mr={`px(6)} />
      <Text textStyle=TextStyles.h2 color=Colors.grey1>
        {currency.name |> React.string}
      </Text>
    </Flex>
    {currentCurrency
     |> Utils.map(currentCurrency => currency === currentCurrency)
     |> Utils.orDefault(false)
     |> Utils.renderIf(
          <Text color=Colors.blue fontSize={`px(16)}>
            <i className="icon-checked" />
          </Text>,
        )}
  </Flex>;
};

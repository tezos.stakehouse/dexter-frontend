type t =
  | Exchange
  | ExchangeAndSend
  | AddLiquidity
  | RemoveLiquidity;

[@react.component]
let make = (~disabled: bool=false, ~size: int=19, ~transactionType: t) =>
  <Flex className={Css.style([Css.opacity(disabled ? 0.33 : 1.)])}>
    <DexterUi_Icon
      darkModeSuffix="-d"
      size
      name={
        switch (transactionType) {
        | Exchange => "exchange"
        | ExchangeAndSend => "send"
        | AddLiquidity => "add"
        | RemoveLiquidity => "remove"
        }
      }
    />
  </Flex>;

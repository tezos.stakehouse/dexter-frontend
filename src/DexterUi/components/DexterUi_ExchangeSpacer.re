[@react.component]
let make = (~onClick: option(_ => unit)=?) => {
  let {isSm} = DexterUiContext.Responsive.useDevice();
  let {darkMode} = DexterUiContext.Theme.useContext();
  let isExchange = onClick |> Belt.Option.isSome;
  let iconSize =
    switch (isSm, isExchange) {
    | (true, true) => 40
    | (true, false) => 28
    | (false, true) => 30
    | (false, false) => 18
    };

  <Flex
    position=`relative
    width={isSm ? `percent(100.) : `px(iconSize)}
    height={isSm ? `px(iconSize) : `initial}
    mt={`px(isSm ? iconSize + 12 : 26)}
    mx={isSm ? `zero : `px(isExchange ? 28 : 16)}
    flexShrink=0.
    justifyContent=`center>
    <Flex
      width={isSm ? `percent(100.) : `px(1)}
      height={!isSm ? `percent(100.) : `px(1)}
      background={darkMode ? Colors.tabInactiveDark : Colors.lightGrey}
    />
    <Flex
      className=Css.(style([transform(translate(pct(-50.), pct(-50.)))]))
      top={isSm ? `zero : isExchange ? `percent(50.) : `px(111)}
      left={`percent(50.)}
      position=`absolute
      background={Colors.boxBackground(darkMode)}
      width={isSm ? `calc((`add, `px(iconSize), `px(16))) : `px(iconSize)}
      height={
        !isSm ? `calc((`add, `px(iconSize), `px(16))) : `px(iconSize)
      }
      alignItems=`center
      justifyContent=`center>
      <Flex
        className=Css.(style(isSm ? [transform(rotate(deg(90.)))] : []))
        ?onClick>
        <DexterUi_Icon
          darkModeSuffix="-d"
          size=iconSize
          name={
            switch (isExchange) {
            | true => "exchange-middle"
            | _ => "receive"
            }
          }
        />
      </Flex>
    </Flex>
  </Flex>;
};

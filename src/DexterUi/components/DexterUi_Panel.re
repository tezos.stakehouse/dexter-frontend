type t =
  | Gray
  | Blue
  | Orange
  | Red;

[@react.component]
let make = (~children: React.element, ~variant: t=Gray) => {
  let {darkMode} = DexterUiContext.Theme.useContext();

  let (infoTextColor, infoBorderColor, infoBackgroundColor) =
    switch (variant) {
    | Gray => (
        darkMode ? Colors.offWhite : Colors.blackish1,
        darkMode ? Colors.offBlack : Colors.offWhite,
        darkMode ? Colors.offBlack : Colors.offWhite,
      )
    | Blue => (
        darkMode ? Colors.offWhite : Colors.blue,
        darkMode ? Colors.primaryDark : Colors.blue,
        darkMode ? Colors.tabInactiveDark : Colors.whiteGray,
      )
    | Orange => (
        darkMode ? Colors.offWhite : Colors.orange,
        darkMode ? Colors.orange : Colors.orange,
        darkMode ? Colors.darkOrange : Colors.lightOrange,
      )
    | Red => (
        darkMode ? Colors.offWhite : Colors.red,
        darkMode ? Colors.red : Colors.red,
        darkMode ? Colors.darkRed : Colors.lightRed,
      )
    };

  <Flex
    className=Css.(style([border(px(1), `solid, infoBorderColor)]))
    background=infoBackgroundColor
    px={`px(16)}
    py={`px(10)}
    borderRadius={`px(8)}
    alignItems=`baseline>
    <Text color=infoTextColor textAlign=`center> children </Text>
  </Flex>;
};

[@react.component]
let make = (~token: Dexter_ExchangeBalance.t) => {
  let {darkMode} = DexterUiContext.Theme.useContext();

  let (xtzAmount, tokenAmount) =
    Dexter_AddLiquidity.removeLiquidityXtzTokenOut(
      ~liquidityBurned=token.lqtBalance,
      ~totalLiquidity=token.exchangeTotalLqt,
      ~tokenPool=token.exchangeTokenBalance,
      ~xtzPool=token.exchangeXtz,
      (),
    );

  <Flex
    className=Css.(
      style([
        border(
          px(1),
          `solid,
          darkMode ? Colors.blackish3 : Colors.lightGrey1,
        ),
      ])
    )
    mt={`px(4)}
    flexShrink=0.
    alignItems=`stretch>
    <Flex
      p={`px(6)}
      pl={`px(8)}
      pr={`px(4)}
      height={`percent(100.)}
      justifyContent=`flexStart
      alignItems=`center
      background={darkMode ? Colors.blackish1 : `transparent}
      width={`percent(50.)}>
      <DexterUi_PoolTokenIcon
        icon={token.iconLight}
        iconDark={token.iconDark}
        small=true
      />
      <Flex flexDirection=`column ml={`px(8)}>
        <Text textStyle=TextStyles.bold lightModeColor=Colors.darkGrey2>
          {(token.lqtBalance |> Tezos.Token.toStringWithCommas)
           ++ " "
           |> React.string}
          <DexterUi_PoolTokenSymbol token />
        </Text>
        <Flex>
          <Text textStyle=TextStyles.small darkModeColor=Colors.primaryDark>
            {Printf.sprintf(
               "%.2f",
               Dexter_AddLiquidity.liquidityPercentage(
                 token.lqtBalance,
                 token.exchangeTotalLqt,
               ),
             )
             ++ "% of liquidity pool"
             |> React.string}
          </Text>
        </Flex>
      </Flex>
    </Flex>
    <Flex
      p={`px(8)}
      py={`px(6)}
      justifyContent=`center
      width={`percent(50.)}
      background={darkMode ? Colors.blackish2 : Colors.whiteGray}
      flexDirection=`column
      overflowX=`auto>
      <Flex>
        <Text textStyle=TextStyles.bold lightModeColor=Colors.blackish1>
          {(xtzAmount |> Tezos.Mutez.toTezStringWithCommas)
           ++ " XTZ"
           |> React.string}
        </Text>
        <Text>
          <DexterUi_CurrencyValue
            value={Mutez(xtzAmount)}
            withBrackets=true
          />
        </Text>
      </Flex>
      <Flex>
        <Text textStyle=TextStyles.bold lightModeColor=Colors.blackish1>
          {(tokenAmount |> Tezos.Token.toStringWithCommas)
           ++ " "
           ++ token.symbol
           |> React.string}
        </Text>
        <Text>
          <DexterUi_CurrencyValue
            token
            value={Token(tokenAmount)}
            withBrackets=true
          />
        </Text>
      </Flex>
    </Flex>
  </Flex>;
};

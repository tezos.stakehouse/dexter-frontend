[@react.component]
let make = (~height: Flex.heightType=`px(205)) => {
  let {balances}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();

  <Flex flexDirection=`column flexGrow=1. height>
    {balances |> Dexter_Balance.getPositiveLqt |> List.length === 0
       ? <Flex p={`px(16)}>
           <DexterUi_EmptyMessage>
             {"You do not have any Dexter pool tokens" |> React.string}
           </DexterUi_EmptyMessage>
         </Flex>
       : <>
           <Flex p={`px(16)} pb={`px(4)}>
             <Flex width={`percent(50.)} pl={`px(8)}>
               <Text textStyle=TextStyles.bold lightModeColor=Colors.blackish1>
                 {"Pool tokens" |> React.string}
               </Text>
             </Flex>
             <Flex width={`percent(50.)} pl={`px(8)}>
               <Text textStyle=TextStyles.bold lightModeColor=Colors.blackish1>
                 {"Redeemable for:" |> React.string}
               </Text>
             </Flex>
           </Flex>
           <Flex flexDirection=`column overflowY=`auto p={`px(16)} pt=`zero>
             {balances
              |> Dexter_Balance.getPositiveLqt
              |> List.mapi((i, balance: Dexter_Balance.t) =>
                   switch (balance) {
                   | XtzBalance(_) => React.null
                   | ExchangeBalance(token) =>
                     <DexterUi_TransactionsPoolTokensRow
                       key={string_of_int(i)}
                       token
                     />
                   }
                 )
              |> Array.of_list
              |> React.array}
           </Flex>
         </>}
  </Flex>;
};

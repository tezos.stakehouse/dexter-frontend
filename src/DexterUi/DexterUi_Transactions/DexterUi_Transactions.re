type tab =
  | Transactions
  | PoolTokens;

[@react.component]
let make = (~defaultTab: tab=Transactions) => {
  let {isSm, isMd} = DexterUiContext.Responsive.useDevice();
  let isLoggedIn = DexterUi_Hooks.useIsLoggedIn();
  let (tab, setTab) = React.useState(_ => defaultTab);

  <DexterUi_Box
    height={`px(isMd ? 257 : 242)}
    maxHeight={isMd ? `initial : `px(242)}
    width={isMd && !isSm ? `zero : `initial}
    p=`zero
    tabs={
      <Flex>
        <DexterUi_Tab
          isActive={tab === Transactions}
          onClick={_ => setTab(_ => Transactions)}
          tabText="Recent transactions"
          iconName="transactions-tab"
        />
        <DexterUi_Tab
          isActive={tab === PoolTokens}
          onClick={_ => setTab(_ => PoolTokens)}
          tabText="My pool tokens"
          iconName="pool-tab"
        />
      </Flex>
    }>
    {isLoggedIn
       ? switch (tab) {
         | Transactions =>
           <DexterUi_TransactionsRecent height={`px(isMd ? 221 : 206)} />
         | PoolTokens =>
           <DexterUi_TransactionsPoolTokens height={`px(isMd ? 221 : 206)} />
         }
       : <Flex py={`px(28)} justifyContent=`center width={`percent(100.)}>
           <DexterUi_Message title={"No wallet connected" |> React.string} />
         </Flex>}
  </DexterUi_Box>;
};

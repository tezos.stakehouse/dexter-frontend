[@react.component]
let make = () => {
  let {isSm, isMd} = DexterUiContext.Responsive.useDevice();

  <Flex flexDirection=`column>
    {(isSm && !Common.isMaintenanceMode())
     |> Utils.renderIf(<DexterUi_NavBarButtons />)}
    <DexterUi_MagmaLink />
    {isMd |> Utils.renderIf(<DexterUi_Version />)}
  </Flex>;
};

[@react.component]
let make = () => {
  let isMobile = Common.isMobile();
  let (showMagmaModal, setShowMagmaModal) = React.useState(_ => isMobile);
  let {connectToBeacon}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();

  let toggleShowMagmaModal =
    React.useCallback0(() =>
      setShowMagmaModal(showMagmaModal => !showMagmaModal)
    );

  <>
    {showMagmaModal
     |> Utils.renderIf(<DexterUi_MagmaModal onClose=toggleShowMagmaModal />)}
    <Flex flexDirection=`column width={`percent(100.)}>
      <Flex
        alignItems=`center
        flexDirection=`column
        width={`percent(100.)}
        pt={`px(12)}>
        <DexterUi_Message
          title={"No wallet connected" |> React.string}
          subtitle={
            "Please connect your wallet using the available options."
            |> React.string
          }
        />
        <Flex height={`px(12)} />
        <DexterUi_Button
          px={`px(32)}
          onClick={_ => isMobile ? toggleShowMagmaModal() : connectToBeacon()}>
          {"Connect Wallet" |> React.string}
        </DexterUi_Button>
        <DexterUi_WalletInfoBalancesPlaceholder />
      </Flex>
    </Flex>
  </>;
};

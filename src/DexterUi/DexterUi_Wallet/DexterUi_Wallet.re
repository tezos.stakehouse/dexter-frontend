[@react.component]
let make = () => {
  let {isSm, isMd} = DexterUiContext.Responsive.useDevice();
  let {account}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();

  <DexterUi_Box
    height={isSm ? `initial : `px(257)}
    maxHeight={isSm ? `initial : `px(257)}
    width={isMd && !isSm ? `zero : `initial}
    mb={`px(16)}>
    {switch (account) {
     | Some(account) => <DexterUi_WalletInfo account />
     | _ => <DexterUi_WalletConnect />
     }}
  </DexterUi_Box>;
};

let getLatestTransactionTitle =
    (
      account: option(Dexter_Account.t),
      latestTransaction: option(Dexter_Transaction.t),
    ) =>
  switch (latestTransaction) {
  | Some(transaction) =>
    switch (transaction.transactionType) {
    | Exchange(transactionType) =>
      account
      |> Utils.map((account: Dexter_Account.t) =>
           transactionType.destination
           |> Tezos.Address.isEqual(account.address)
         )
      |> Utils.orDefault(false)
        ? "Exchange" : "Exchange and send"
    | AddLiquidity(_) => "Add Liquidity"
    | RemoveLiquidity(_) => "Remove Liquidity"
    }
  | _ => "Exchange"
  };

[@react.component]
let make = () => {
  let {account} = DexterUiContext.Account.useContext();
  let {transactions, transactionStatus, setTransactionStatus} =
    DexterUiContext.Transactions.useContext();
  let {darkMode} = DexterUiContext.Theme.useContext();

  let latestTransaction =
    transactions |> Dexter_Transaction.getLatestTransaction;

  let (bgColor, fontWeight, text, subtext) =
    switch (transactionStatus) {
    | Applied => (
        Colors.green,
        `semiBold,
        getLatestTransactionTitle(account, latestTransaction)
        ++ " transaction successful!",
        None,
      )
    | Pending => (
        Colors.violet,
        `semiBold,
        "Transaction pending on the Tezos blockchain...",
        None,
      )
    | Failed(reason) when reason === FailureReason.SlippageLimitExceeded => (
        darkMode ? Colors.lightRed2 : Colors.lightRed,
        `semiBold,
        getLatestTransactionTitle(account, latestTransaction)
        ++ " failed due to price slippage.",
        Some("Please try again."),
      )
    | Failed(_)
    | Backtracked
    | Skipped => (
        Colors.red,
        `semiBold,
        getLatestTransactionTitle(account, latestTransaction)
        ++ " transaction failed",
        None,
      )
    | Invalidated => (
        darkMode ? Colors.lightRed2 : Colors.lightRed,
        `semiBold,
        "A new block invalidated your input.",
        None,
      )
    | Ready => (
        darkMode ? Colors.offBlack : Colors.offWhite,
        `normal,
        "Ready for transactions!",
        None,
      )
    };

  let color = {
    switch (transactionStatus) {
    | Invalidated => Some(Colors.red)
    | Failed(reason) when reason === FailureReason.SlippageLimitExceeded =>
      Some(Colors.red)
    | Ready => None
    | _ => Some(Colors.white)
    };
  };

  let borderColor_ = {
    switch (transactionStatus) {
    | Invalidated => Colors.red
    | Failed(reason) when reason === FailureReason.SlippageLimitExceeded => Colors.red
    | _ => bgColor
    };
  };

  let icon = {
    switch (transactionStatus) {
    | Applied =>
      Some(
        <Text ?color fontSize={`px(20)}>
          <i className="icon-checked" />
        </Text>,
      )
    | Failed(reason) when reason === FailureReason.SlippageLimitExceeded =>
      None
    | Failed(_)
    | Backtracked
    | Skipped =>
      Some(
        <Text ?color fontSize={`px(20)}>
          <i className="icon-cancel" />
        </Text>,
      )
    | Pending =>
      Some(<DexterUi_Loader loaderColor=Colors.white loaderSize=20 />)
    | _ => None
    };
  };

  <Flex
    className=Css.(style([border(px(1), `solid, borderColor_)]))
    my={`px(12)}
    pl={`px(icon |> Belt.Option.isSome ? 8 : 16)}
    pr={`px(16)}
    minHeight={`px(36)}
    alignItems=`center
    justifyContent=`spaceBetween
    background=bgColor>
    <Flex alignItems=`center>
      {icon
       |> Utils.renderOpt(icon =>
            <Flex mr={`px(8)}>
              <Text ?color fontSize={`px(20)}> icon </Text>
            </Flex>
          )}
      <Text ?color fontWeight> {text |> React.string} </Text>
      {subtext
       |> Utils.renderOpt(subtext =>
            <Text ?color spaceBefore=true> {subtext |> React.string} </Text>
          )}
    </Flex>
    {switch (transactionStatus) {
     | Pending
     | Ready => React.null
     | _ =>
       <Flex onClick={_ => setTransactionStatus(Ready)}>
         <Text ?color textDecoration=`underline>
           {"Clear" |> React.string}
         </Text>
       </Flex>
     }}
  </Flex>;
};

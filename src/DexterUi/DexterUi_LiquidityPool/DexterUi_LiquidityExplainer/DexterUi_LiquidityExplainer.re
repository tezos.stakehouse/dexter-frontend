[@react.component]
let make = () => {
  let {isXxs, isSm} = DexterUiContext.Responsive.useDevice();
  let {darkMode} = DexterUiContext.Theme.useContext();
  let (explainerVisible, setExplainerVisible) = React.useState(_ => false);

  <>
    <Flex
      position=`absolute
      top={`px(11)}
      right={`px(isXxs ? 11 : 15)}
      onClick={_ => setExplainerVisible(_ => !explainerVisible)}>
      <Text
        fontSize={`px(15)}
        darkModeColor=Colors.white
        lightModeColor=Colors.black>
        <i className="icon-info" />
      </Text>
    </Flex>
    {explainerVisible
     |> Utils.renderIf(
          <Flex
            className=Css.(
              style([
                border(
                  px(1),
                  `solid,
                  darkMode ? Colors.tabInactiveDark : Colors.lightGrey1,
                ),
              ])
            )
            mt={`px(-2)}
            px={`px(18)}
            py={`px(14)}
            background={darkMode ? Colors.offBlack : Colors.whiteGray}
            flexDirection=`column
            position=`absolute
            right={`px(isSm ? 16 : 20)}
            left={`px(isSm ? 16 : 20)}
            maxHeight={`calc((`sub, `percent(100.), `px(isSm ? 66 : 70)))}
            alignItems=`center
            zIndex=3
            overflowY=`auto>
            <Flex
              mb={Css.px(12)}
              width={`percent(100.)}
              justifyContent=`spaceBetween>
              <Flex alignItems={isSm ? `flexStart : `center}>
                <Flex mr={`px(8)}>
                  <Text fontSize={`px(15)} lightModeColor=Colors.blackish1>
                    <i
                      className=Css.(
                        merge([
                          "icon-info",
                          style([before([important(margin(`zero))])]),
                        ])
                      )
                    />
                  </Text>
                </Flex>
                <Flex flexDirection={isSm ? `column : `row}>
                  <Text
                    textStyle=TextStyles.h3 lightModeColor=Colors.blackish1>
                    {"Dexter Liquidity Pool" |> React.string}
                  </Text>
                  <Flex ml={`px(8)} />
                  <Text>
                    {"Provide liquidity to the Dexter exchange to earn a share of global exchange fees"
                     |> React.string}
                  </Text>
                </Flex>
              </Flex>
              <Flex onClick={_ => setExplainerVisible(_ => !explainerVisible)}>
                <Text
                  darkModeColor=Colors.primaryDark
                  lightModeColor=Colors.blue
                  textDecoration=`underline>
                  {"Close" |> React.string}
                </Text>
              </Flex>
            </Flex>
            <DexterUi_LiquidityExplainerInfo />
            <DexterUi_Link
              ellipsis=false
              href="https://dexter.exchange/docs/liquidity-pools-intro/">
              {"Read more about how Liquidity Pools work" |> React.string}
            </DexterUi_Link>
          </Flex>,
        )}
  </>;
};

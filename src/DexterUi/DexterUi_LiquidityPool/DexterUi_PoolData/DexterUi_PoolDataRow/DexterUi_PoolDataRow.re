[@react.component]
let make = (~token: Dexter_ExchangeBalance.t) =>
  <Flex mt={`px(8)}>
    <Flex width={`percent(23.)} alignItems=`center pr={`px(12)}>
      <DexterUi_PoolTokenIcon
        icon={token.iconLight}
        iconDark={token.iconDark}
        small=true
      />
      <Flex width={`px(12)} />
      <Text textStyle=TextStyles.bold lightModeColor=Colors.darkGrey2>
        <DexterUi_PoolTokenSymbol token />
      </Text>
    </Flex>
    <Flex width={`percent(19.)} alignItems=`center pr={`px(12)}>
      <Text>
        {(token.exchangeXtz |> Tezos.Mutez.toTezStringWithCommasTruncated(2))
         ++ " XTZ"
         |> React.string}
      </Text>
    </Flex>
    <Flex width={`percent(19.)} alignItems=`center pr={`px(12)}>
      <Text>
        {(
           token.exchangeTokenBalance
           |> Tezos.Token.toStringWithCommasTruncated(
                token.symbol === "tzBTC" ? 4 : 2,
              )
         )
         ++ " "
         ++ token.symbol
         |> React.string}
      </Text>
    </Flex>
    <Flex width={`percent(19.)} alignItems=`center pr={`px(12)}>
      <Text>
        {switch (token.dexterBaker) {
         | Some(dexterBaker) =>
           <DexterUi_Link
             href={
               Dexter_Settings.tzktUrl
               ++ (dexterBaker |> Tezos_Address.toString)
             }>
             {Dexter_Settings.knownBakers
              |> List.find_opt((baker: Dexter_Settings.baker) =>
                   baker.address === (dexterBaker |> Tezos_Address.toString)
                 )
              |> Utils.map((baker: Dexter_Settings.baker) => baker.name)
              |> Utils.orDefault(dexterBaker |> Tezos_Address.toString)
              |> React.string}
           </DexterUi_Link>
         | _ => "No baker" |> React.string
         }}
      </Text>
    </Flex>
    <Flex width={`percent(20.)} alignItems=`center justifyContent=`center>
      <DexterUi_PoolDataRowButtons token />
    </Flex>
  </Flex>;

[@react.component]
let make = (~token: Dexter_ExchangeBalance.t) =>
  <Flex mt={`px(8)} flexDirection=`column>
    <Flex full=true alignItems=`center justifyContent=`spaceBetween>
      <Flex alignItems=`center>
        <DexterUi_PoolTokenIcon
          icon={token.iconLight}
          iconDark={token.iconDark}
          small=true
        />
        <Flex width={`px(12)} />
        <Text textStyle=TextStyles.bold lightModeColor=Colors.darkGrey2>
          <DexterUi_PoolTokenSymbol token />
        </Text>
      </Flex>
      <Flex> <DexterUi_PoolDataRowButtons token /> </Flex>
    </Flex>
    <Flex alignItems=`center mt={`px(4)}>
      <Text>
        {(token.exchangeXtz |> Tezos.Mutez.toTezStringWithCommas)
         ++ " XTZ/"
         ++ (token.exchangeTokenBalance |> Tezos.Token.toStringWithCommas)
         ++ " "
         ++ token.symbol
         |> React.string}
      </Text>
    </Flex>
    <Flex full=true alignItems=`center mt={`px(4)}>
      <Text>
        {switch (token.dexterBaker) {
         | Some(dexterBaker) =>
           <DexterUi_Link
             href={
               Dexter_Settings.tzktUrl
               ++ (dexterBaker |> Tezos_Address.toString)
             }>
             {Dexter_Settings.knownBakers
              |> List.find_opt((baker: Dexter_Settings.baker) =>
                   baker.address === (dexterBaker |> Tezos_Address.toString)
                 )
              |> Utils.map((baker: Dexter_Settings.baker) => baker.name)
              |> Utils.orDefault(dexterBaker |> Tezos_Address.toString)
              |> React.string}
           </DexterUi_Link>
         | _ => "No baker" |> React.string
         }}
      </Text>
    </Flex>
  </Flex>;

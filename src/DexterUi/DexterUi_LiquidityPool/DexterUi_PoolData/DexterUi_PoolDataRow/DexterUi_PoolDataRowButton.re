[@react.component]
let make = (~disabled: bool=false, ~hash: string, ~route: Dexter_Route.t) => {
  let {isSm} = DexterUiContext.Responsive.useDevice();
  let {setRoute} = DexterUiContext.Route.useContext();
  let isLoggedIn = DexterUi_Hooks.useIsLoggedIn();
  let disabled = !isLoggedIn || disabled;
  let onClick = !disabled ? Some(_ => setRoute(route, Some(hash))) : None;

  <Flex ?onClick mr={`px(8)}>
    <DexterUi_TransactionIcon
      disabled
      size={isSm ? 22 : 19}
      transactionType={
        switch (route) {
        | Dexter_Route.LiquidityPool(AddLiquidity) => AddLiquidity
        | Dexter_Route.LiquidityPool(RemoveLiquidity) => RemoveLiquidity
        | _ => Exchange
        }
      }
    />
  </Flex>;
};

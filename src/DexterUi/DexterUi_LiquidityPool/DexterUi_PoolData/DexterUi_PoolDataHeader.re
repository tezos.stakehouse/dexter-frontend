[@react.component]
let make = () =>
  <Flex mb={`px(4)}>
    <Flex width={`percent(23.)}>
      <Text lightModeColor=Colors.blackish1 textStyle=TextStyles.bold>
        {"Exchange pair" |> React.string}
      </Text>
    </Flex>
    <Flex width={`percent(19.)}>
      <Text lightModeColor=Colors.blackish1 textStyle=TextStyles.bold>
        {"Tokens pooled" |> React.string}
      </Text>
    </Flex>
    <Flex width={`percent(19.)}>
      <Text lightModeColor=Colors.blackish1 textStyle=TextStyles.bold>
        {"Tokens pooled" |> React.string}
      </Text>
    </Flex>
    <Flex width={`percent(19.)}>
      <Text lightModeColor=Colors.blackish1 textStyle=TextStyles.bold>
        {"Contract baker" |> React.string}
      </Text>
    </Flex>
    <Flex width={`percent(20.)}>
      <Text lightModeColor=Colors.blackish1 textStyle=TextStyles.bold>
        {"Add / Remove / Exchange" |> React.string}
      </Text>
    </Flex>
  </Flex>;

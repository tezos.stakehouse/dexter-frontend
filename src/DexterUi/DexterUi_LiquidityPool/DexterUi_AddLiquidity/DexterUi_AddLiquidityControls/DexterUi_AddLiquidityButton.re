open Utils;
open DexterUi_AddLiquidity_Utils;

[@react.component]
let make =
    (
      ~disabled: bool,
      ~resetInputs: unit => unit,
      ~state: DexterUi_AddLiquidity_Reducer.state,
    ) => {
  let {account} = DexterUiContext.Account.useContext();
  let {pushTransaction, transactionTimeout} =
    DexterUiContext.Transactions.useContext();

  state.token
  |> renderOpt((token: Dexter_ExchangeBalance.t) =>
       <DexterUi_TransactionButton
         px={`px(32)}
         disabled
         onClick={_ =>
           switch (account) {
           | Some(account) =>
             onAddLiquidity(
               account,
               pushTransaction,
               resetInputs,
               state,
               token,
               transactionTimeout,
             )
           | _ => ()
           }
         }>
         {"Add liquidity" |> React.string}
       </DexterUi_TransactionButton>
     );
};

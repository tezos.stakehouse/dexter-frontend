type state = {
  liquidity: Valid.t(Tezos.Token.t),
  token: option(Dexter_ExchangeBalance.t),
};

type action =
  | UpdateLiquidityValue(Valid.t(Tezos.Token.t))
  | UpdatePoolToken(Dexter_ExchangeBalance.t)
  | ResetInputs
  | ResetTokens;

let useRemoveLiquidityReducer = tokens => {
  let positiveTokens = tokens |> Dexter_Balance.getPositiveLqt;

  let initialToken =
    switch (DexterUi_Hooks.useTokensFromLocalStorage(positiveTokens)) {
    | (Some(ExchangeBalance(token)), _)
    | (_, Some(ExchangeBalance(token))) => Some(token)
    | _ =>
      positiveTokens |> List.length > 0
        ? positiveTokens |> Dexter.Balance.getFirstToken1
        : tokens |> Dexter.Balance.getFirstToken1
    };

  let initialState = {
    liquidity: Dexter_Value.tokenZeroValue,
    token: initialToken,
  };

  React.useReducer(
    (state, action) =>
      switch (action) {
      | UpdateLiquidityValue(liquidity) => {...state, liquidity}
      | UpdatePoolToken(token) => {
          token: Some(token),
          liquidity: Dexter_Value.tokenZeroValue,
        }
      | ResetInputs => {...state, liquidity: Dexter_Value.tokenZeroValue}
      | ResetTokens => {...state, token: initialToken}
      },
    initialState,
  );
};

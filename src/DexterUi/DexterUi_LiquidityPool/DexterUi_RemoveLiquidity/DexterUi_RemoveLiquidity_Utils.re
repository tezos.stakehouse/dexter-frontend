/** encodeRemoveLiquidity
 *
 * lqtBurned
 * minMutezWithdrawn
 * minTokenWithdrawn
 * pair (pair (address :owner) (pair (address :to) (nat :lqtBurned)))  (pair (mutez :minXtzWithdrawn) (pair (nat :minTokensWithdrawn) (timestamp :deadline)))
 */
let encodeRemoveLiquidity =
    (
      ~owner: Tezos.Address.t,
      ~to_: Tezos.Address.t,
      ~lqtBurned: Tezos.Token.t,
      ~minMutezWithdrawn: Tezos.Mutez.t,
      ~minTokenWithdrawn: Tezos.Token.t,
      ~deadline: Tezos.Timestamp.t,
      (),
    ) =>
  Tezos.(
    Expression.SingleExpression(
      Primitives.PrimitiveData(PrimitiveData.Pair),
      Some([
        Expression.SingleExpression(
          Primitives.PrimitiveData(PrimitiveData.Pair),
          Some([
            Expression.StringExpression(Tezos.Address.toString(owner)),
            Expression.SingleExpression(
              Primitives.PrimitiveData(PrimitiveData.Pair),
              Some([
                Expression.StringExpression(Tezos.Address.toString(to_)),
                Expression.IntExpression(lqtBurned.value),
              ]),
              None,
            ),
          ]),
          None,
        ),
        Expression.SingleExpression(
          Primitives.PrimitiveData(PrimitiveData.Pair),
          Some([
            Expression.IntExpression(
              Tezos.Mutez.toBigint(minMutezWithdrawn),
            ),
            Expression.SingleExpression(
              Primitives.PrimitiveData(PrimitiveData.Pair),
              Some([
                Expression.IntExpression(minTokenWithdrawn.value),
                Expression.StringExpression(
                  Tezos.Timestamp.toString(deadline),
                ),
              ]),
              None,
            ),
          ]),
          None,
        ),
      ]),
      None,
    )
  );

let removeLiquidity =
    (
      ~contract: Tezos.Contract.t,
      ~owner: Tezos.Address.t,
      ~to_: Tezos.Address.t,
      ~lqtBurned: Tezos.Token.t,
      ~minMutezWithdrawn: Tezos.Mutez.t,
      ~minTokenWithdrawn: Tezos.Token.t,
      ~deadline: Tezos.Timestamp.t,
      ~wallet: Tezos_Wallet.t,
      (),
    )
    : Js.Promise.t(Belt.Result.t(Tezos_Wallet.Operation.Response.t, string)) => {
  switch (wallet) {
  | Beacon(client) =>
    Beacon.postTransaction(
      client,
      [
        {
          kind: Tezos_Operation.Kind.Transaction,
          destination: contract,
          amount: Tezos.Mutez.zero,
          parameters:
            Some({
              entrypoint: "removeLiquidity",
              value:
                encodeRemoveLiquidity(
                  ~owner,
                  ~to_,
                  ~lqtBurned,
                  ~minMutezWithdrawn=
                    Tezos.Mutez.subPointThreePercent(minMutezWithdrawn),
                  ~minTokenWithdrawn=
                    Tezos.Token.subPointThreePercent(minTokenWithdrawn),
                  ~deadline,
                  (),
                ),
            }),
        },
      ],
    )
    |> Js.Promise.then_(result => {
         switch (result) {
         | None =>
           Belt.Result.Error("network not found") |> Js.Promise.resolve
         | Some(result) =>
           Belt.Result.Ok(Tezos_Wallet.Operation.Response.Beacon(result))
           |> Js.Promise.resolve
         }
       })
  };
};

let onRemoveLiquidity =
    (
      account: Dexter_Account.t,
      pushTransaction: Dexter_Transaction.t => unit,
      resetInputs: unit => unit,
      state: DexterUi_RemoveLiquidity_Reducer.state,
      tokenAmount: Tezos.Token.t,
      transactionTimeout: int,
      xtzAmount: Tezos.Mutez.t,
    ) => {
  switch (state.liquidity, state.token) {
  | (Valid(lqtBurned, _), Some(token)) =>
    let owner = account.address;
    let (minMutezWithdrawn, minTokenWithdrawn) =
      Dexter_AddLiquidity.removeLiquidityXtzTokenOut(
        ~liquidityBurned=lqtBurned,
        ~totalLiquidity=token.exchangeTotalLqt,
        ~tokenPool=token.exchangeTokenBalance,
        ~xtzPool=token.exchangeXtz,
        (),
      );
    let dexterContract = token.dexterContract;
    let deadline = Tezos.Timestamp.minutesFromNow(transactionTimeout);

    removeLiquidity(
      ~contract=dexterContract,
      ~owner,
      ~to_=owner,
      ~lqtBurned,
      ~minMutezWithdrawn,
      ~minTokenWithdrawn,
      ~deadline,
      ~wallet=account.wallet,
      (),
    )
    |> Js.Promise.then_(result => {
         switch (
           (result: Belt.Result.t(Tezos_Wallet.Operation.Response.t, string))
         ) {
         | Belt.Result.Ok(response) =>
           resetInputs();

           Dexter_Transaction.ofTransactionResponse(
             dexterContract,
             response,
             account.address,
             Dexter_TransactionType.RemoveLiquidity({
               lqtBurned,
               symbol: token.symbol,
               tokenOut: tokenAmount,
               xtzOut: xtzAmount,
             }),
           )
           |> Js.Promise.then_(result => {
                switch (result) {
                | Belt.Result.Ok(transaction) => pushTransaction(transaction)
                | Belt.Result.Error(error) => Js.log(error)
                };
                Js.Promise.resolve();
              })
           |> ignore;

         | Belt.Result.Error(error) => Js.log(error)
         };

         Js.Promise.resolve();
       })
    |> Js.Promise.catch(error => {
         Js.log(error);
         ErrorReporting.Sentry.capturePromiseError(
           "removeLiquidity failed. ",
           error,
         );
         Js.Promise.resolve();
       })
    |> ignore;

  | _ => ()
  };
};

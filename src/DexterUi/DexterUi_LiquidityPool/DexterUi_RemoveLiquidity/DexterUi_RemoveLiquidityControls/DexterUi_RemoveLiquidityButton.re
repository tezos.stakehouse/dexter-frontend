open DexterUi_RemoveLiquidity_Utils;

[@react.component]
let make =
    (
      ~disabled: bool,
      ~resetInputs: unit => unit,
      ~state: DexterUi_RemoveLiquidity_Reducer.state,
      ~tokenAmount: Tezos.Token.t,
      ~xtzAmount: Tezos.Mutez.t,
    ) => {
  let {account} = DexterUiContext.Account.useContext();
  let {pushTransaction, transactionTimeout} =
    DexterUiContext.Transactions.useContext();

  <DexterUi_TransactionButton
    disabled
    px={`px(20)}
    onClick={_ =>
      switch (account) {
      | Some(account) =>
        onRemoveLiquidity(
          account,
          pushTransaction,
          resetInputs,
          state,
          tokenAmount,
          transactionTimeout,
          xtzAmount,
        )
      | _ => ()
      }
    }>
    {"Redeem pool tokens" |> React.string}
  </DexterUi_TransactionButton>;
};
